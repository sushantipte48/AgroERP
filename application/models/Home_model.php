<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home_model extends CI_Model {

	public function load_products($value='')
	{
		$this->db->select('*');
		$this->db->from('product p');
		if($value!="")
		{
			$this->db->where('p.pcat', $value);
		}
		$this->db->where('p.status', '1');
		return $this->db->get()->result();
	}

	public function get_offer($value='')
	{
		$this->db->select('*');
		$this->db->from('offer o');
		$this->db->where('o.pid', $value);
		$this->db->where("o.from < NOW()");
		$this->db->where("o.to > NOW()");
		$this->db->where('o.status','1');
		return $this->db->get()->row();
	}

}

/* End of file Home_model.php */
/* Location: ./application/models/Home_model.php */