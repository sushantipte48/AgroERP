<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Output extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Output_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		$this->data['module'] = 'output/output_list';
		$this->load->view('layout/datatable', $this->data);
		//$this->load->view('output/output_list');
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Output_model->json();
	}

	public function read($id) {
		$row = $this->Output_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'       => $row->id,
				'shop'     => $row->shop,
				'oprice'   => $row->oprice,
				'stock'    => $row->stock,
				'products' => $row->products,
				'oid'      => $row->oid,
				'status'   => $row->status,
				'odate'    => $row->odate,
			);
			$this->data['module'] = 'output/output_read';
			$this->load->view('layout/datatable', $this->data);
			$this->load->view('output/output_read', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('output'));
		}
	}

	public function create() {
		$this->data = array(
			'button'   => 'Create',
			'action'   => site_url('output/create_action'),
			'id'       => set_value('id'),
			'shop'     => set_value('shop'),
			'oprice'   => set_value('oprice'),
			'stock'    => set_value('stock'),
			'products' => set_value('products'),
			'oid'      => set_value('oid'),
			'status'   => set_value('status'),
			'odate'    => set_value('odate'),
		);
		$this->data['offer']=$this->Output_model->get_aaa();
		$this->data['module'] = 'output/output_form';
		$this->load->view('layout/form', $this->data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'id'       => $this->input->post('id', TRUE),
				'shop'     => $this->input->post('shop', TRUE),
				'oprice'   => $this->input->post('oprice', TRUE),
				'stock'    => $this->input->post('stock', TRUE),
				'products' => $this->input->post('products', TRUE),
				'oid'      => $this->input->post('oid', TRUE),
				'status'   => $this->input->post('status', TRUE),
				'odate'    => $this->input->post('odate', TRUE),
			);

			$this->Output_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('output'));
		}
	}

	public function update($id) {
		$row = $this->Output_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button'   => 'Update',
				'action'   => site_url('output/update_action'),
				'id'       => set_value('id', $row->id),
				'shop'     => set_value('shop', $row->shop),
				'oprice'   => set_value('oprice', $row->oprice),
				'stock'    => set_value('stock', $row->stock),
				'products' => set_value('products', $row->products),
				'oid'      => set_value('oid', $row->oid),
				'status'   => set_value('status', $row->status),
				'odate'    => set_value('odate', $row->odate),
			);
			$this->data['offer']=$this->Output_model->get_aaa();
			$this->data['module'] = 'output/output_form';
			$this->load->view('layout/form', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('output'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('', TRUE));
		} else {
			$data = array(
				'id'       => $this->input->post('id', TRUE),
				'shop'     => $this->input->post('shop', TRUE),
				'oprice'   => $this->input->post('oprice', TRUE),
				'stock'    => $this->input->post('stock', TRUE),
				'products' => $this->input->post('products', TRUE),
				'oid'      => $this->input->post('oid', TRUE),
				'status'   => $this->input->post('status', TRUE),
				'odate'    => $this->input->post('odate', TRUE),
			);

			$this->Output_model->update($this->input->post('', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('output'));
		}
	}

	public function delete($id) {
		$row = $this->Output_model->get_by_id($id);

		if ($row) {
			$this->Output_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('output'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('output'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('id', 'id', 'trim|required');
		$this->form_validation->set_rules('shop', 'shop', 'trim|required');
		$this->form_validation->set_rules('oprice', 'oprice', 'trim|required');
		$this->form_validation->set_rules('stock', 'stock', 'trim|required');
		$this->form_validation->set_rules('products', 'products', 'trim|required');
		$this->form_validation->set_rules('oid', 'oid', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('odate', 'odate', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "output.xls";
		$judul     = "output";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Id");
		xlsWriteLabel($tablehead, $kolomhead++, "Shop");
		xlsWriteLabel($tablehead, $kolomhead++, "Oprice");
		xlsWriteLabel($tablehead, $kolomhead++, "Stock");
		xlsWriteLabel($tablehead, $kolomhead++, "Products");
		xlsWriteLabel($tablehead, $kolomhead++, "Oid");
		xlsWriteLabel($tablehead, $kolomhead++, "Status");
		xlsWriteLabel($tablehead, $kolomhead++, "Odate");

		foreach ($this->Output_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteNumber($tablebody, $kolombody++, $data->id);
			xlsWriteNumber($tablebody, $kolombody++, $data->shop);
			xlsWriteNumber($tablebody, $kolombody++, $data->oprice);
			xlsWriteNumber($tablebody, $kolombody++, $data->stock);
			xlsWriteLabel($tablebody, $kolombody++, $data->products);
			xlsWriteNumber($tablebody, $kolombody++, $data->oid);
			xlsWriteNumber($tablebody, $kolombody++, $data->status);
			xlsWriteLabel($tablebody, $kolombody++, $data->odate);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

}

/* End of file Output.php */
/* Location: ./application/controllers/Output.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:10:24 */
/* http://harviacode.com */