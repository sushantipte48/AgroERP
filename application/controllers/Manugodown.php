<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Manugodown extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Manugodown_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		$this->data['module'] = 'manugodown/manugodown_list';
		$this->load->view('layout/datatable', $this->data);
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Manugodown_model->json();
	}

	public function read($id) {
		$row = $this->Manugodown_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'      => $row->id,
				'mid'     => $row->mid,
				'gadd'    => $row->gadd,
				'contact' => $row->contact,
				'status'  => $row->status,
			);
			$this->data['module'] = 'manugodown/manugodown_read';
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('manugodown'));
		}
	}

	public function create() {
		$this->data = array(
			'button'  => 'Create',
			'action'  => site_url('manugodown/create_action'),
			'id'      => set_value('id'),
			'mid'     => set_value('mid'),
			'gadd'    => set_value('gadd'),
			'contact' => set_value('contact'),
			'status'  => set_value('status'),
		);
		$this->data['manu1'] = $this->Manugodown_model->get_manu1();
		$this->data['module'] = 'manugodown/manugodown_form';
		$this->load->view('layout/datatable', $this->data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'mid'     => $this->input->post('mid', TRUE),
				'gadd'    => $this->input->post('gadd', TRUE),
				'contact' => $this->input->post('contact', TRUE),
				'status'  => $this->input->post('status', TRUE),
			);

			$this->Manugodown_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('manugodown'));
		}
	}

	public function update($id) {
		$row = $this->Manugodown_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button'  => 'Update',
				'action'  => site_url('manugodown/update_action'),
				'id'      => set_value('id', $row->id),
				'mid'     => set_value('mid', $row->mid),
				'gadd'    => set_value('gadd', $row->gadd),
				'contact' => set_value('contact', $row->contact),
				'status'  => set_value('status', $row->status),
			);
			$this->data['manu1'] = $this->Manugodown_model->get_manu1();
			$this->data['module'] = 'manugodown/manugodown_form';
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('manugodown'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'mid'     => $this->input->post('mid', TRUE),
				'gadd'    => $this->input->post('gadd', TRUE),
				'contact' => $this->input->post('contact', TRUE),
				'status'  => $this->input->post('status', TRUE),
			);

			$this->Manugodown_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('manugodown'));
		}
	}

	public function delete($id) {
		$row = $this->Manugodown_model->get_by_id($id);

		if ($row) {
			$this->Manugodown_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('manugodown'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('manugodown'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('mid', 'mid', 'trim|required');
		$this->form_validation->set_rules('gadd', 'gadd', 'trim|required');
		$this->form_validation->set_rules('contact', 'contact', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "manugodown.xls";
		$judul     = "manugodown";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Mid");
		xlsWriteLabel($tablehead, $kolomhead++, "Gadd");
		xlsWriteLabel($tablehead, $kolomhead++, "Contact");
		xlsWriteLabel($tablehead, $kolomhead++, "Status");

		foreach ($this->Manugodown_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteNumber($tablebody, $kolombody++, $data->mid);
			xlsWriteLabel($tablebody, $kolombody++, $data->gadd);
			xlsWriteNumber($tablebody, $kolombody++, $data->contact);
			xlsWriteNumber($tablebody, $kolombody++, $data->status);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

}

/* End of file Manugodown.php */
/* Location: ./application/controllers/Manugodown.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:09:47 */
/* http://harviacode.com */