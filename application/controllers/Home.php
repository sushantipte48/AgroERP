<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
	public function __construct()
	{
		parent::__construct();
		$this->load->model('Home_model');
	}

	public function index()
	{
		$this->load->view('home/index');
	}

	public function about()
	{
		$this->load->view('home/about');
	}

	public function contact()
	{
		$this->load->view('home/contact');
	}

	public function products($value='')
	{
		// $this->load->view('marketplace/category');
		$this->fetch();
	}

	public function services()
	{
		$this->load->view('home/services');
	}

	public function fetch($value='')
	{
		$prod = $this->Home_model->load_products($value);
		$data ="";
		$temp ="";
		$i=0;
		foreach ($prod as $key) {
			$price = "";
			$offer = $this->Home_model->get_offer($key->id);
			if($offer!="")
			{
				if($offer->disc_type == 1)
				{
					$price = $key->psell - $offer->discount;
				}
				elseif ($offer->disc_type == 2) {
					$price = $key->psell - ($offer->discount/$key->psell*100);
				}
				
			}
			else
			{
				$price = $key->psell;
			}
			if($key->psell < $key->pmrp)
			{
				$temp = '<div class="ribbon sale">
                            <div class="theribbon">SALE</div>
                            <div class="ribbon-background"></div>
                        </div>';
			}
			if($price!=$key->psell)
			{
				$temp = $temp.'<div class="ribbon new">
                                    <div class="theribbon">OFFER</div>
                                    <div class="ribbon-background"></div>
                                </div>';
			}
			if($key->front_pic == NULL || $key->front_pic == "")
			{
				$img1 = '<img src="'.base_url('assets/img/products/front/')."NoPicAvailable.png".'" alt="" style="max-height: 300px;max-width: 252px" class="img-responsive">';
			}
			else
			{
				$img1 = '<img src="'.base_url('assets/img/products/front/').$key->front_pic.'" alt="" style="max-height: 300px;max-width: 252px" class="img-responsive">';
			}
			if($key->back_pic == NULL || $key->front_pic == "")
			{
				$img2 = '<img src="'.base_url('assets/img/products/back/')."NoPicAvailable.png".'" alt="" style="max-height: 300px;max-width: 252px" class="img-responsive">';

			}
			else
			{
				$img2 = '<img src="'.base_url('assets/img/products/back/').$key->back_pic.'" alt="" style="max-height: 300px;max-width: 252px" class="img-responsive">';
			}
			$data = $data.' <div class="col-md-4 col-sm-6">
                            <div class="product">
                                <div class="flip-container">
                                    <div class="flipper">
                                        <div class="front" style="text-align: center;">
                                            '.$img1.'
                                        </div>
                                        <div class="back" style="text-align: center;">
                                                '.$img2.'
                                        </div>
                                    </div>
                                </div>
                                <a class="invisible">
                                    <img src="" style="height : 300px" alt="" class="img-responsive">
                                </a>
                                <div class="text">
                                    <h3>'.$key->pname.'</h3>
                                    <p >'.$key->pdesc.'</p>
                                    <table><tr>
                                    <div><td>MRP</td><td>:</td><td><d style="text-decoration: line-through;">$'.$key->pmrp.'.00</d></td></div>
                                    </tr><tr>
                                    <div><td>Our Offer</td><td>:</td><td><b>$'.$price.'.00</b></td></div>
                                    </tr>
                                    </table>
                                    <p class="buttons">
                                        <a href="basket.html" class="btn btn-primary"><i class="fa fa-shopping-cart"></i>Add to cart</a>
                                    </p>
                                </div>
                                <!-- /.text -->
                                '.$temp.'
                            </div>
                            <!-- /.product -->
                        </div>';
            $i++;
            if($i==3)
            {
            	$data = $data."<br>";
            	$i=0;
            }
		}

		$this->data['market'] = $data;
		$this->load->view('marketplace/category',$this->data);
		
	}

}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */