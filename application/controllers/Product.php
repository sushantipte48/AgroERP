<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Product extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Product_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		$this->data['module'] = 'product/product_list';
		$this->load->view('layout/datatable', $this->data);
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Product_model->json();
	}

	public function read($id) {
		$row = $this->Product_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'          => $row->id,
				'pname'       => $row->pname,
				'pdesc'       => $row->pdesc,
				'pcat'        => $row->pcat,
				'pcost'       => $row->pcost,
				'pmrp'        => $row->pmrp,
				'psell'       => $row->psell,
				'created_on'  => $row->created_on,
				'created_by'  => $row->created_by,
				'modified_on' => $row->modified_on,
				'modified_by' => $row->modified_by,
				'status'      => $row->status,
			);

			$this->data['module'] = 'product/product_read';
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('product'));
		}
	}

	public function create() {
		$this->data = array(
			'button'      => 'Create',
			'action'      => site_url('product/create_action'),
			'id'          => set_value('id'),
			'maunufacturerid' => set_value('maunufacturerid'),
			'godownid' => set_value('godownid'),
			'pname'       => set_value('pname'),
			'pdesc'       => set_value('pdesc'),
			'pcat'        => set_value('pcat'),
			'pcost'       => set_value('pcost'),
			'pmrp'        => set_value('pmrp'),
			'psell'       => set_value('psell'),
			'created_on'  => set_value('created_on'),
			'created_by'  => set_value('created_by'),
			'modified_on' => set_value('modified_on'),
			'modified_by' => set_value('modified_by'),
			'status'      => set_value('status'),
		);
		$this->data['pro']    = $this->Product_model->get_pro();
		$this->data['module'] = 'product/product_form';
		$this->load->view('layout/form', $this->data);
		//$this->load->view('product/product_form', $data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$additional = array();
			$newname    = date('Ymdhis');
			if ($_FILES['front']['name'] != NULL) {

				$ext                     = pathinfo(basename($_FILES["file"]["name"]), PATHINFO_EXTENSION);
				$config['upload_path']   = base_url('assets/img/').'front/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name']     = $newname;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('file')) {
					$error = array('error' => $this->upload->display_errors());
					echo $this->upload->display_errors('<p>', '</p>');
					print_r($this->upload->data());
				}
				$additional = array_merge($additional, array('front_pic' => $newname.'.'.$ext));
			}
			if ($_FILES['back']['name'] != NULL) {
				$ext                     = pathinfo(basename($_FILES["file"]["name"]), PATHINFO_EXTENSION);
				$config['upload_path']   = base_url('assets/img/').'back/';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name']     = $newname;
				$this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('file')) {
					$error = array('error' => $this->upload->display_errors());
					echo $this->upload->display_errors('<p>', '</p>');
					print_r($this->upload->data());
				}
				$additional = array_merge($additional, array('back_pic' => $newname.'.'.$ext));
			}

			$data = array(
				'pname'      => $this->input->post('pname', TRUE),
				'pdesc'      => $this->input->post('pdesc', TRUE),
				'pcat'       => $this->input->post('pcat', TRUE),
				'pcost'      => $this->input->post('pcost', TRUE),
				'pmrp'       => $this->input->post('pmrp', TRUE),
				'psell'      => $this->input->post('psell', TRUE),
				'created_on' => date('Y-m-d H:i:s'),
				'created_by' => $this->session->userdata('user_id'),
				'status'     => $this->input->post('status', TRUE),
			);
			$data = array_merge($data, $additional);
			$this->Product_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('product'));
		}
	}

	public function update($id) {
		$row = $this->Product_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button'      => 'Update',
				'action'      => site_url('product/update_action'),
				'id'          => set_value('id', $row->id),
				'pname'       => set_value('pname', $row->pname),
				'pdesc'       => set_value('pdesc', $row->pdesc),
				'pcat'        => set_value('pcat', $row->pcat),
				'pcost'       => set_value('pcost', $row->pcost),
				'pmrp'        => set_value('pmrp', $row->pmrp),
				'psell'       => set_value('psell', $row->psell),
				'created_on'  => set_value('created_on', $row->created_on),
				'created_by'  => set_value('created_by', $row->created_by),
				'modified_on' => set_value('modified_on', $row->modified_on),
				'modified_by' => set_value('modified_by', $row->modified_by),
				'status'      => set_value('status', $row->status),
			);
			$this->data['pro']    = $this->Product_model->get_pro();
			$this->data['module'] = 'product/product_form';
			$this->load->view('layout/form', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('product'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$additional = array();
			$newname    = date('Ymdhis');
			if ($_FILES['front']['name'] != NULL) {
				$ext                     = pathinfo(basename($_FILES["front"]["name"]), PATHINFO_EXTENSION);
				$config['upload_path']   = './assets/img/products/front';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name']     = $newname.".".$ext;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('front')) {
					$error = array('error' => $this->upload->display_errors());
					echo $this->upload->display_errors('<p>', '</p>');
					print_r($this->upload->data());
					exit();
				} else {
					$additional = array_merge($additional, array('front_pic' => $newname.'.'.$ext));
				}

			}
			if ($_FILES['back']['name'] != NULL) {

				$ext                     = pathinfo(basename($_FILES["back"]["name"]), PATHINFO_EXTENSION);
				$config['upload_path']   = './assets/img/products/back';
				$config['allowed_types'] = 'gif|jpg|png';
				$config['file_name']     = $newname.".".$ext;
				$this->upload->initialize($config);
				// $this->load->library('upload', $config);
				$this->upload->initialize($config);
				if (!$this->upload->do_upload('back')) {
					$error = array('error' => $this->upload->display_errors());
					echo $this->upload->display_errors('<p>', '</p>');
					print_r($this->upload->data());
					exit();
				} else {
					$additional = array_merge($additional, array('back_pic' => $newname.'.'.$ext));
				}

			}
			$data = array(
				'pname'       => $this->input->post('pname', TRUE),
				'pdesc'       => $this->input->post('pdesc', TRUE),
				'pcat'        => $this->input->post('pcat', TRUE),
				'pcost'       => $this->input->post('pcost', TRUE),
				'pmrp'        => $this->input->post('pmrp', TRUE),
				'psell'       => $this->input->post('psell', TRUE),
				'modified_on' => date('Y-m-d H:i:s'),
				'modified_by' => $this->session->userdata('user_id'),
				'status'      => $this->input->post('status', TRUE),
			);
			$data = array_merge($data, $additional);
			$this->Product_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('product'));
		}
	}

	public function delete($id) {
		$row = $this->Product_model->get_by_id($id);

		if ($row) {
			$this->Product_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('product'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('product'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('pname', 'pname', 'trim|required');
		$this->form_validation->set_rules('pdesc', 'pdesc', 'trim|required');
		$this->form_validation->set_rules('pcat', 'pcat', 'trim|required');
		$this->form_validation->set_rules('pcost', 'pcost', 'trim|required');
		$this->form_validation->set_rules('pmrp', 'pmrp', 'trim|required');
		$this->form_validation->set_rules('psell', 'psell', 'trim|required');
		$this->form_validation->set_rules('created_on', 'created on', 'trim');
		$this->form_validation->set_rules('created_by', 'created by', 'trim');
		$this->form_validation->set_rules('modified_on', 'modified on', 'trim');
		$this->form_validation->set_rules('modified_by', 'modified by', 'trim');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "product.xls";
		$judul     = "product";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Pname");
		xlsWriteLabel($tablehead, $kolomhead++, "Pdesc");
		xlsWriteLabel($tablehead, $kolomhead++, "Pcat");
		xlsWriteLabel($tablehead, $kolomhead++, "Pcost");
		xlsWriteLabel($tablehead, $kolomhead++, "Pmrp");
		xlsWriteLabel($tablehead, $kolomhead++, "Psell");
		xlsWriteLabel($tablehead, $kolomhead++, "Created On");
		xlsWriteLabel($tablehead, $kolomhead++, "Created By");
		xlsWriteLabel($tablehead, $kolomhead++, "Modified On");
		xlsWriteLabel($tablehead, $kolomhead++, "Modified By");
		xlsWriteLabel($tablehead, $kolomhead++, "Status");

		foreach ($this->Product_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->pname);
			xlsWriteLabel($tablebody, $kolombody++, $data->pdesc);
			xlsWriteNumber($tablebody, $kolombody++, $data->pcat);
			xlsWriteNumber($tablebody, $kolombody++, $data->pcost);
			xlsWriteNumber($tablebody, $kolombody++, $data->pmrp);
			xlsWriteNumber($tablebody, $kolombody++, $data->psell);
			xlsWriteLabel($tablebody, $kolombody++, $data->created_on);
			xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
			xlsWriteLabel($tablebody, $kolombody++, $data->modified_on);
			xlsWriteNumber($tablebody, $kolombody++, $data->modified_by);
			xlsWriteNumber($tablebody, $kolombody++, $data->status);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

	public function FunctionName($value = '') {
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size']      = '100';
		$config['max_width']     = '1024';
		$config['max_height']    = '768';

		$this->load->library('upload', $config);

		if (!$this->upload->do_upload()) {
			$error = array('error' => $this->upload->display_errors());
		} else {
			$data = array('upload_data' => $this->upload->data());
			echo "success";
		}
	}

}

/* End of file Product.php */
/* Location: ./application/controllers/Product.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:07:52 */
/* http://harviacode.com */