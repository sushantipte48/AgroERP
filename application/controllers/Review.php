<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Review extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Review_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		$this->data['module'] = 'review/review_list';
		$this->load->view('layout/datatable', $this->data);
		//$this->load->view('review/review_list');
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Review_model->json();
	}

	public function read($id) {
		$row = $this->Review_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'     => $row->id,
				'pid'    => $row->pid,
				'rating' => $row->rating,
				'desc'   => $row->desc,
				'status' => $row->status,
			);
			$this->data['module'] = 'review/review_read';
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('review'));
		}
	}

	public function create() {
		$this->data = array(
			'button' => 'Create',
			'action' => site_url('review/create_action'),
			'id'     => set_value('id'),
			'pid'    => set_value('pid'),
			'rating' => set_value('rating'),
			'desc'   => set_value('desc'),
			'status' => set_value('status'),
		);
		$this->data['pro2'] = $this->Review_model->get_pro2();
		$this->data['module'] = 'review/review_form';
		$this->load->view('layout/form', $this->data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'pid'    => $this->input->post('pid', TRUE),
				'rating' => $this->input->post('rating', TRUE),
				'desc'   => $this->input->post('desc', TRUE),
				'status' => $this->input->post('status', TRUE),
			);

			$this->Review_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('review'));
		}
	}

	public function update($id) {
		$row = $this->Review_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button' => 'Update',
				'action' => site_url('review/update_action'),
				'id'     => set_value('id', $row->id),
				'pid'    => set_value('pid', $row->pid),
				'rating' => set_value('rating', $row->rating),
				'desc'   => set_value('desc', $row->desc),
				'status' => set_value('status', $row->status),
			);
			$this->data['pro2'] = $this->Review_model->get_pro2();
			$this->data['module'] = 'review/review_form';
			$this->load->view('layout/form', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('review'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'pid'    => $this->input->post('pid', TRUE),
				'rating' => $this->input->post('rating', TRUE),
				'desc'   => $this->input->post('desc', TRUE),
				'status' => $this->input->post('status', TRUE),
			);

			$this->Review_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('review'));
		}
	}

	public function delete($id) {
		$row = $this->Review_model->get_by_id($id);

		if ($row) {
			$this->Review_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('review'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('review'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('pid', 'pid', 'trim|required');
		$this->form_validation->set_rules('rating', 'rating', 'trim|required');
		$this->form_validation->set_rules('desc', 'desc', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "review.xls";
		$judul     = "review";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Pid");
		xlsWriteLabel($tablehead, $kolomhead++, "Rating");
		xlsWriteLabel($tablehead, $kolomhead++, "Desc");
		xlsWriteLabel($tablehead, $kolomhead++, "Status");

		foreach ($this->Review_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteNumber($tablebody, $kolombody++, $data->pid);
			xlsWriteNumber($tablebody, $kolombody++, $data->rating);
			xlsWriteLabel($tablebody, $kolombody++, $data->desc);
			xlsWriteNumber($tablebody, $kolombody++, $data->status);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

}

/* End of file Review.php */
/* Location: ./application/controllers/Review.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:16:26 */
/* http://harviacode.com */