<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Keys extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Keys_model');
        $this->load->library('form_validation');        
	    $this->load->library('datatables');
    }

    public function index()
    {
        if ($this->ion_auth->is_admin()) 
        {
            $this->data['module']="keys/keys_list";
            $this->load->view('layout/datatable', $this->data);
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Keys_model->json();
    }

    public function read($id) 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $row = $this->Keys_model->get_by_id($id);
            if ($row) {
                $data = array(
                    'id' => $row->id,
                    'user_id' => $row->user_id,
                    'key' => $row->key,
                    'level' => $row->level,
                    'ignore_limits' => $row->ignore_limits,
                    'is_private_key' => $row->is_private_key,
                    'ip_addresses' => $row->ip_addresses,
                    'date_created' => $row->date_created,
                );
                $this->load->view('keys/keys_read', $data);
            } else {
                $this->session->set_flashdata('message', 'Record Not Found');
                redirect(site_url('keys'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
    }

    public function create() 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $data = array(
                'button' => 'Create',
                'action' => site_url('keys/create_action'),
                'id' => set_value('id'),
                'user_id' => set_value('user_id'),
                'key' => set_value('key'),
                'level' => set_value('level'),
                'ignore_limits' => set_value('ignore_limits'),
                'is_private_key' => set_value('is_private_key'),
                'ip_addresses' => set_value('ip_addresses'),
                'date_created' => set_value('date_created'),
            );
            $this->load->view('keys/keys_form', $data);
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }

            
    }
    
    public function create_action() 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $this->_rules();

            if ($this->form_validation->run() == FALSE) {
                $this->create();
            } else {
                $data = array(
                    'user_id' => $this->input->post('user_id',TRUE),
                    'key' => $this->input->post('key',TRUE),
                    'level' => $this->input->post('level',TRUE),
                    'ignore_limits' => $this->input->post('ignore_limits',TRUE),
                    'is_private_key' => $this->input->post('is_private_key',TRUE),
                    'ip_addresses' => $this->input->post('ip_addresses',TRUE),
                    'date_created' => $this->input->post('date_created',TRUE),
                );

                $this->Keys_model->insert($data);
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('keys'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
            
    }
    
    public function update($id) 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $row = $this->Keys_model->get_by_id($id);

            if ($row) {
                $data = array(
                    'button' => 'Update',
                    'action' => site_url('keys/update_action'),
                    'id' => set_value('id', $row->id),
                    'user_id' => set_value('user_id', $row->user_id),
                    'key' => set_value('key', $row->key),
                    'level' => set_value('level', $row->level),
                    'ignore_limits' => set_value('ignore_limits', $row->ignore_limits),
                    'is_private_key' => set_value('is_private_key', $row->is_private_key),
                    'ip_addresses' => set_value('ip_addresses', $row->ip_addresses),
                    'date_created' => set_value('date_created', $row->date_created),
                );
                $this->load->view('keys/keys_form', $data);
            } else {
                $this->session->set_flashdata('message', 'Record Not Found');
                redirect(site_url('keys'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
            
    }
    
    public function update_action() 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $this->_rules();
            if ($this->form_validation->run() == FALSE) {
                $this->update($this->input->post('id', TRUE));
            } else {
                $data = array(
                    'user_id' => $this->input->post('user_id',TRUE),
                    'key' => $this->input->post('key',TRUE),
                    'level' => $this->input->post('level',TRUE),
                    'ignore_limits' => $this->input->post('ignore_limits',TRUE),
                    'is_private_key' => $this->input->post('is_private_key',TRUE),
                    'ip_addresses' => $this->input->post('ip_addresses',TRUE),
                    'date_created' => $this->input->post('date_created',TRUE),
                    );
                $this->Keys_model->update($this->input->post('id', TRUE), $data);
                $this->session->set_flashdata('message', 'Update Record Success');
                redirect(site_url('keys'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }

            
    }
    
    public function delete($id) 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $row = $this->Keys_model->get_by_id($id);

            if ($row) {
                $this->Keys_model->delete($id);
                $this->session->set_flashdata('message', 'Delete Record Success');
                redirect(site_url('keys'));
            } else {
                $this->session->set_flashdata('message', 'Record Not Found');
                redirect(site_url('keys'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
            
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('user_id', 'user id', 'trim|required');
	$this->form_validation->set_rules('key', 'key', 'trim|required');
	$this->form_validation->set_rules('level', 'level', 'trim|required');
	$this->form_validation->set_rules('ignore_limits', 'ignore limits', 'trim|required');
	$this->form_validation->set_rules('is_private_key', 'is private key', 'trim|required');
	$this->form_validation->set_rules('ip_addresses', 'ip addresses', 'trim|required');
	$this->form_validation->set_rules('date_created', 'date created', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        if ($this->ion_auth->is_admin()) 
        {
            $this->load->helper('exportexcel');
            $namaFile = "keys.xls";
            $judul = "keys";
            $tablehead = 0;
            $tablebody = 1;
            $nourut = 1;
            //penulisan header
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-Disposition: attachment;filename=" . $namaFile . "");
            header("Content-Transfer-Encoding: binary ");
            xlsBOF();
            $kolomhead = 0;
            xlsWriteLabel($tablehead, $kolomhead++, "No");
            xlsWriteLabel($tablehead, $kolomhead++, "User Id");
            xlsWriteLabel($tablehead, $kolomhead++, "Key");
            xlsWriteLabel($tablehead, $kolomhead++, "Level");
            xlsWriteLabel($tablehead, $kolomhead++, "Ignore Limits");
            xlsWriteLabel($tablehead, $kolomhead++, "Is Private Key");
            xlsWriteLabel($tablehead, $kolomhead++, "Ip Addresses");
            xlsWriteLabel($tablehead, $kolomhead++, "Date Created");
            foreach ($this->Keys_model->get_all() as $data) {
                $kolombody = 0;
                //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
                xlsWriteNumber($tablebody, $kolombody++, $nourut);
                xlsWriteNumber($tablebody, $kolombody++, $data->user_id);
                xlsWriteLabel($tablebody, $kolombody++, $data->key);
                xlsWriteNumber($tablebody, $kolombody++, $data->level);
                xlsWriteLabel($tablebody, $kolombody++, $data->ignore_limits);
                xlsWriteLabel($tablebody, $kolombody++, $data->is_private_key);
                xlsWriteLabel($tablebody, $kolombody++, $data->ip_addresses);
                xlsWriteNumber($tablebody, $kolombody++, $data->date_created);
                $tablebody++;
                $nourut++;
            }
            xlsEOF();
            exit();
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
    }
}

/* End of file Keys.php */
/* Location: ./application/controllers/Keys.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-06-15 10:57:40 */
/* http://harviacode.com */