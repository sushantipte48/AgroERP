<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Agrogodown extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Agrogodown_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		$this->data['module'] = "agrogodown/agrogodown_list";
		$this->load->view('layout/datatable', $this->data);
		//$this->load->view('agrogodown/agrogodown_list');
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Agrogodown_model->json();
	}

	public function read($id) {
		$row = $this->Agrogodown_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'           => $row->id,
				'agro_add'     => $row->agro_add,
				'agro_contact' => $row->agro_contact,
				'capacity'     => $row->capacity,
			);
			$this->data['module'] = "agrogodown/agrogodown_read";
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('agrogodown'));
		}
	}

	public function create() {
		$this->data = array(
			'button'       => 'Create',
			'action'       => site_url('agrogodown/create_action'),
			'id'           => set_value('id'),
			'agro_add'     => set_value('agro_add'),
			'agro_contact' => set_value('agro_contact'),
			'capacity'     => set_value('capacity'),
		);
		$this->data['module'] = "agrogodown/agrogodown_form";
		$this->load->view('layout/form', $this->data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'agro_add'     => $this->input->post('agro_add', TRUE),
				'agro_contact' => $this->input->post('agro_contact', TRUE),
				'capacity'     => $this->input->post('capacity', TRUE),
			);

			$this->Agrogodown_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('agrogodown'));
		}
	}

	public function update($id) {
		$row = $this->Agrogodown_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button'       => 'Update',
				'action'       => site_url('agrogodown/update_action'),
				'id'           => set_value('id', $row->id),
				'agro_add'     => set_value('agro_add', $row->agro_add),
				'agro_contact' => set_value('agro_contact', $row->agro_contact),
				'capacity'     => set_value('capacity', $row->capacity),
			);
			$this->data['module'] = "agrogodown/agrogodown_form";
			$this->load->view('layout/form', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('agrogodown'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'agro_add'     => $this->input->post('agro_add', TRUE),
				'agro_contact' => $this->input->post('agro_contact', TRUE),
				'capacity'     => $this->input->post('capacity', TRUE),
			);

			$this->Agrogodown_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('agrogodown'));
		}
	}

	public function delete($id) {
		$row = $this->Agrogodown_model->get_by_id($id);

		if ($row) {
			$this->Agrogodown_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('agrogodown'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('agrogodown'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('agro_add', 'agro add', 'trim|required');
		$this->form_validation->set_rules('agro_contact', 'agro contact', 'trim|required');
		$this->form_validation->set_rules('capacity', 'capacity', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "agrogodown.xls";
		$judul     = "agrogodown";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Agro Add");
		xlsWriteLabel($tablehead, $kolomhead++, "Agro Contact");
		xlsWriteLabel($tablehead, $kolomhead++, "Capacity");

		foreach ($this->Agrogodown_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->agro_add);
			xlsWriteNumber($tablebody, $kolombody++, $data->agro_contact);
			xlsWriteNumber($tablebody, $kolombody++, $data->capacity);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

}

/* End of file Agrogodown.php */
/* Location: ./application/controllers/Agrogodown.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:06:55 */
/* http://harviacode.com */