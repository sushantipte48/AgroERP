<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Logs extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Logs_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('logs/logs_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Logs_model->json();
    }

    public function read($id) 
    {
        $row = $this->Logs_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'uri' => $row->uri,
		'method' => $row->method,
		'params' => $row->params,
		'api_key' => $row->api_key,
		'ip_address' => $row->ip_address,
		'time' => $row->time,
		'rtime' => $row->rtime,
		'authorized' => $row->authorized,
		'response_code' => $row->response_code,
	    );
            $this->load->view('logs/logs_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('logs'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('logs/create_action'),
	    'id' => set_value('id'),
	    'uri' => set_value('uri'),
	    'method' => set_value('method'),
	    'params' => set_value('params'),
	    'api_key' => set_value('api_key'),
	    'ip_address' => set_value('ip_address'),
	    'time' => set_value('time'),
	    'rtime' => set_value('rtime'),
	    'authorized' => set_value('authorized'),
	    'response_code' => set_value('response_code'),
	);
        $this->load->view('logs/logs_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'uri' => $this->input->post('uri',TRUE),
		'method' => $this->input->post('method',TRUE),
		'params' => $this->input->post('params',TRUE),
		'api_key' => $this->input->post('api_key',TRUE),
		'ip_address' => $this->input->post('ip_address',TRUE),
		'time' => $this->input->post('time',TRUE),
		'rtime' => $this->input->post('rtime',TRUE),
		'authorized' => $this->input->post('authorized',TRUE),
		'response_code' => $this->input->post('response_code',TRUE),
	    );

            $this->Logs_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('logs'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Logs_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('logs/update_action'),
		'id' => set_value('id', $row->id),
		'uri' => set_value('uri', $row->uri),
		'method' => set_value('method', $row->method),
		'params' => set_value('params', $row->params),
		'api_key' => set_value('api_key', $row->api_key),
		'ip_address' => set_value('ip_address', $row->ip_address),
		'time' => set_value('time', $row->time),
		'rtime' => set_value('rtime', $row->rtime),
		'authorized' => set_value('authorized', $row->authorized),
		'response_code' => set_value('response_code', $row->response_code),
	    );
            $this->load->view('logs/logs_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('logs'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'uri' => $this->input->post('uri',TRUE),
		'method' => $this->input->post('method',TRUE),
		'params' => $this->input->post('params',TRUE),
		'api_key' => $this->input->post('api_key',TRUE),
		'ip_address' => $this->input->post('ip_address',TRUE),
		'time' => $this->input->post('time',TRUE),
		'rtime' => $this->input->post('rtime',TRUE),
		'authorized' => $this->input->post('authorized',TRUE),
		'response_code' => $this->input->post('response_code',TRUE),
	    );

            $this->Logs_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('logs'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Logs_model->get_by_id($id);

        if ($row) {
            $this->Logs_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('logs'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('logs'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('uri', 'uri', 'trim|required');
	$this->form_validation->set_rules('method', 'method', 'trim|required');
	$this->form_validation->set_rules('params', 'params', 'trim|required');
	$this->form_validation->set_rules('api_key', 'api key', 'trim|required');
	$this->form_validation->set_rules('ip_address', 'ip address', 'trim|required');
	$this->form_validation->set_rules('time', 'time', 'trim|required');
	$this->form_validation->set_rules('rtime', 'rtime', 'trim|required');
	$this->form_validation->set_rules('authorized', 'authorized', 'trim|required');
	$this->form_validation->set_rules('response_code', 'response code', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "logs.xls";
        $judul = "logs";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Uri");
	xlsWriteLabel($tablehead, $kolomhead++, "Method");
	xlsWriteLabel($tablehead, $kolomhead++, "Params");
	xlsWriteLabel($tablehead, $kolomhead++, "Api Key");
	xlsWriteLabel($tablehead, $kolomhead++, "Ip Address");
	xlsWriteLabel($tablehead, $kolomhead++, "Time");
	xlsWriteLabel($tablehead, $kolomhead++, "Rtime");
	xlsWriteLabel($tablehead, $kolomhead++, "Authorized");
	xlsWriteLabel($tablehead, $kolomhead++, "Response Code");

	foreach ($this->Logs_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->uri);
	    xlsWriteLabel($tablebody, $kolombody++, $data->method);
	    xlsWriteLabel($tablebody, $kolombody++, $data->params);
	    xlsWriteLabel($tablebody, $kolombody++, $data->api_key);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ip_address);
	    xlsWriteNumber($tablebody, $kolombody++, $data->time);
	    xlsWriteLabel($tablebody, $kolombody++, $data->rtime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->authorized);
	    xlsWriteLabel($tablebody, $kolombody++, $data->response_code);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Logs.php */
/* Location: ./application/controllers/Logs.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-06-15 10:57:41 */
/* http://harviacode.com */