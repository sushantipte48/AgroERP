<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Access extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Access_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('access/access_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Access_model->json();
    }

    public function read($id) 
    {
        $row = $this->Access_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'key' => $row->key,
		'all_access' => $row->all_access,
		'controller' => $row->controller,
		'date_created' => $row->date_created,
		'date_modified' => $row->date_modified,
	    );
            $this->load->view('access/access_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('access'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('access/create_action'),
	    'id' => set_value('id'),
	    'key' => set_value('key'),
	    'all_access' => set_value('all_access'),
	    'controller' => set_value('controller'),
	    'date_created' => set_value('date_created'),
	    'date_modified' => set_value('date_modified'),
	);
        $this->load->view('access/access_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'key' => $this->input->post('key',TRUE),
		'all_access' => $this->input->post('all_access',TRUE),
		'controller' => $this->input->post('controller',TRUE),
		'date_created' => $this->input->post('date_created',TRUE),
		'date_modified' => $this->input->post('date_modified',TRUE),
	    );

            $this->Access_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('access'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Access_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('access/update_action'),
		'id' => set_value('id', $row->id),
		'key' => set_value('key', $row->key),
		'all_access' => set_value('all_access', $row->all_access),
		'controller' => set_value('controller', $row->controller),
		'date_created' => set_value('date_created', $row->date_created),
		'date_modified' => set_value('date_modified', $row->date_modified),
	    );
            $this->load->view('access/access_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('access'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'key' => $this->input->post('key',TRUE),
		'all_access' => $this->input->post('all_access',TRUE),
		'controller' => $this->input->post('controller',TRUE),
		'date_created' => $this->input->post('date_created',TRUE),
		'date_modified' => $this->input->post('date_modified',TRUE),
	    );

            $this->Access_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('access'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Access_model->get_by_id($id);

        if ($row) {
            $this->Access_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('access'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('access'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('key', 'key', 'trim|required');
	$this->form_validation->set_rules('all_access', 'all access', 'trim|required');
	$this->form_validation->set_rules('controller', 'controller', 'trim|required');
	$this->form_validation->set_rules('date_created', 'date created', 'trim|required');
	$this->form_validation->set_rules('date_modified', 'date modified', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "access.xls";
        $judul = "access";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Key");
	xlsWriteLabel($tablehead, $kolomhead++, "All Access");
	xlsWriteLabel($tablehead, $kolomhead++, "Controller");
	xlsWriteLabel($tablehead, $kolomhead++, "Date Created");
	xlsWriteLabel($tablehead, $kolomhead++, "Date Modified");

	foreach ($this->Access_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->key);
	    xlsWriteLabel($tablebody, $kolombody++, $data->all_access);
	    xlsWriteLabel($tablebody, $kolombody++, $data->controller);
	    xlsWriteLabel($tablebody, $kolombody++, $data->date_created);
	    xlsWriteLabel($tablebody, $kolombody++, $data->date_modified);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Access.php */
/* Location: ./application/controllers/Access.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-06-15 10:57:40 */
/* http://harviacode.com */