<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Limits extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Limits_model');
        $this->load->library('form_validation');        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('limits/limits_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Limits_model->json();
    }

    public function read($id) 
    {
        $row = $this->Limits_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'uri' => $row->uri,
		'count' => $row->count,
		'hour_started' => $row->hour_started,
		'api_key' => $row->api_key,
	    );
            $this->load->view('limits/limits_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('limits'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('limits/create_action'),
	    'id' => set_value('id'),
	    'uri' => set_value('uri'),
	    'count' => set_value('count'),
	    'hour_started' => set_value('hour_started'),
	    'api_key' => set_value('api_key'),
	);
        $this->load->view('limits/limits_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'uri' => $this->input->post('uri',TRUE),
		'count' => $this->input->post('count',TRUE),
		'hour_started' => $this->input->post('hour_started',TRUE),
		'api_key' => $this->input->post('api_key',TRUE),
	    );

            $this->Limits_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('limits'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Limits_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('limits/update_action'),
		'id' => set_value('id', $row->id),
		'uri' => set_value('uri', $row->uri),
		'count' => set_value('count', $row->count),
		'hour_started' => set_value('hour_started', $row->hour_started),
		'api_key' => set_value('api_key', $row->api_key),
	    );
            $this->load->view('limits/limits_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('limits'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'uri' => $this->input->post('uri',TRUE),
		'count' => $this->input->post('count',TRUE),
		'hour_started' => $this->input->post('hour_started',TRUE),
		'api_key' => $this->input->post('api_key',TRUE),
	    );

            $this->Limits_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('limits'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Limits_model->get_by_id($id);

        if ($row) {
            $this->Limits_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('limits'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('limits'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('uri', 'uri', 'trim|required');
	$this->form_validation->set_rules('count', 'count', 'trim|required');
	$this->form_validation->set_rules('hour_started', 'hour started', 'trim|required');
	$this->form_validation->set_rules('api_key', 'api key', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "limits.xls";
        $judul = "limits";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Uri");
	xlsWriteLabel($tablehead, $kolomhead++, "Count");
	xlsWriteLabel($tablehead, $kolomhead++, "Hour Started");
	xlsWriteLabel($tablehead, $kolomhead++, "Api Key");

	foreach ($this->Limits_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->uri);
	    xlsWriteNumber($tablebody, $kolombody++, $data->count);
	    xlsWriteNumber($tablebody, $kolombody++, $data->hour_started);
	    xlsWriteLabel($tablebody, $kolombody++, $data->api_key);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Limits.php */
/* Location: ./application/controllers/Limits.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-06-15 10:57:40 */
/* http://harviacode.com */