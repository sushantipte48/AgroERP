<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Stock extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Stock_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		$this->data['module'] = 'stock/stock_list';
		$this->load->view('layout/datatable', $this->data);
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Stock_model->json();
	}

	public function read($id) {
		$row = $this->Stock_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'       => $row->id,
				'gid'      => $row->gid,
				'pid'      => $row->pid,
				'stock'    => $row->stock,
				'exp_date' => $row->exp_date,
			);
			$this->data['module'] = 'stock/stock_read';
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('stock'));
		}
	}

	public function create() {
		$this->data = array(
			'button'   => 'Create',
			'action'   => site_url('stock/create_action'),
			'id'       => set_value('id'),
			'gid'      => set_value('gid'),
			'pid'      => set_value('pid'),
			'stock'    => set_value('stock'),
			'exp_date' => set_value('exp_date'),
		);
		$this->data['pro1'] = $this->Stock_model->get_pro1();
		$this->data['go1'] = $this->Stock_model->get_go1();
		$this->data['module'] = 'stock/stock_form';
		$this->load->view('layout/form', $this->data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'gid'      => $this->input->post('gid', TRUE),
				'pid'      => $this->input->post('pid', TRUE),
				'stock'    => $this->input->post('stock', TRUE),
				'exp_date' => $this->input->post('exp_date', TRUE),
			);

			$this->Stock_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('stock'));
		}
	}

	public function update($id) {
		$row = $this->Stock_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button'   => 'Update',
				'action'   => site_url('stock/update_action'),
				'id'       => set_value('id', $row->id),
				'gid'      => set_value('gid', $row->gid),
				'pid'      => set_value('pid', $row->pid),
				'stock'    => set_value('stock', $row->stock),
				'exp_date' => set_value('exp_date', $row->exp_date),
			);
			
			$this->data['pro1'] = $this->Stock_model->get_pro1();
			$this->data['go1'] = $this->Stock_model->get_go1();
			$this->data['module'] = 'stock/stock_form';
			$this->load->view('layout/form', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('stock'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'gid'      => $this->input->post('gid', TRUE),
				'pid'      => $this->input->post('pid', TRUE),
				'stock'    => $this->input->post('stock', TRUE),
				'exp_date' => $this->input->post('exp_date', TRUE),
			);

			$this->Stock_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('stock'));
		}
	}

	public function delete($id) {
		$row = $this->Stock_model->get_by_id($id);

		if ($row) {
			$this->Stock_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('stock'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('stock'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('gid', 'gid', 'trim|required');
		$this->form_validation->set_rules('pid', 'pid', 'trim|required');
		$this->form_validation->set_rules('stock', 'stock', 'trim|required');
		$this->form_validation->set_rules('exp_date', 'exp date', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "stock.xls";
		$judul     = "stock";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Gid");
		xlsWriteLabel($tablehead, $kolomhead++, "Pid");
		xlsWriteLabel($tablehead, $kolomhead++, "Stock");
		xlsWriteLabel($tablehead, $kolomhead++, "Exp Date");

		foreach ($this->Stock_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteNumber($tablebody, $kolombody++, $data->gid);
			xlsWriteNumber($tablebody, $kolombody++, $data->pid);
			xlsWriteNumber($tablebody, $kolombody++, $data->stock);
			xlsWriteLabel($tablebody, $kolombody++, $data->exp_date);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

}

/* End of file Stock.php */
/* Location: ./application/controllers/Stock.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:13:29 */
/* http://harviacode.com */