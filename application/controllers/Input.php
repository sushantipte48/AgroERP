<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Input extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Input_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		$this->data['module'] = "input/input_list";
		$this->load->view('layout/datatable', $this->data);
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Input_model->json();
	}

	public function read($id) {
		$row = $this->Input_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'            => $row->id,
				'mid'           => $row->mid,
				'mgodown'       => $row->mgodown,
				'product'       => $row->product,
				'stock'         => $row->stock,
				'price'         => $row->price,
				'ordered_on'    => $row->ordered_on,
				'received_date' => $row->received_date,
				'status'        => $row->status,
			);
			$this->data['module'] = 'input/input_read';
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('input'));
		}
	}

	public function create() {
		$this->data = array(
			'button'        => 'Create',
			'action'        => site_url('input/create_action'),
			'id'            => set_value('id'),
			'mid'           => set_value('mid'),
			'mgodown'       => set_value('mgodown'),
			'product'       => set_value('product'),
			'stock'         => set_value('stock'),
			'price'         => set_value('price'),
			'ordered_on'    => set_value('ordered_on'),
			'received_date' => set_value('received_date'),
			'status'        => set_value('status'),
		);
		$this->data['manu']=$this->Input_model->get_manu();
		$this->data['godown'] = $this->Input_model->get_godown();
		$this->data['123'] = $this->Input_model->get_123();
		$this->data['module'] = 'input/input_form';
		$this->load->view('layout/form', $this->data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'mid'           => $this->input->post('mid', TRUE),
				'mgodown'       => $this->input->post('mgodown', TRUE),
				'product'       => $this->input->post('product', TRUE),
				'stock'         => $this->input->post('stock', TRUE),
				'price'         => $this->input->post('price', TRUE),
				'ordered_on'    => $this->input->post('ordered_on', TRUE),
				'received_date' => $this->input->post('received_date', TRUE),
				'status'        => $this->input->post('status', TRUE),
			);

			$this->Input_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('input'));
		}
	}

	public function update($id) {
		$row = $this->Input_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button'        => 'Update',
				'action'        => site_url('input/update_action'),
				'id'            => set_value('id', $row->id),
				'mid'           => set_value('mid', $row->mid),
				'mgodown'       => set_value('mgodown', $row->mgodown),
				'product'       => set_value('product', $row->product),
				'stock'         => set_value('stock', $row->stock),
				'price'         => set_value('price', $row->price),
				'ordered_on'    => set_value('ordered_on', $row->ordered_on),
				'received_date' => set_value('received_date', $row->received_date),
				'status'        => set_value('status', $row->status),
			);

			$this->data['manu']=$this->Input_model->get_manu();
			$this->data['godown'] = $this->Input_model->get_godown();
			$this->data['123'] = $this->Input_model->get_123();
			$this->data['module'] = 'input/input_form';
			$this->load->view('layout/form', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('input'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'mid'           => $this->input->post('mid', TRUE),
				'mgodown'       => $this->input->post('mgodown', TRUE),
				'product'       => $this->input->post('product', TRUE),
				'stock'         => $this->input->post('stock', TRUE),
				'price'         => $this->input->post('price', TRUE),
				'ordered_on'    => $this->input->post('ordered_on', TRUE),
				'received_date' => $this->input->post('received_date', TRUE),
				'status'        => $this->input->post('status', TRUE),
			);

			$this->Input_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('input'));
		}
	}

	public function delete($id) {
		$row = $this->Input_model->get_by_id($id);

		if ($row) {
			$this->Input_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('input'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('input'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('mid', 'mid', 'trim|required');
		$this->form_validation->set_rules('mgodown', 'mgodown', 'trim|required');
		$this->form_validation->set_rules('product', 'product', 'trim|required');
		$this->form_validation->set_rules('stock', 'stock', 'trim|required');
		$this->form_validation->set_rules('price', 'price', 'trim|required');
		$this->form_validation->set_rules('ordered_on', 'ordered on', 'trim|required');
		$this->form_validation->set_rules('received_date', 'received date', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "input.xls";
		$judul     = "input";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Mid");
		xlsWriteLabel($tablehead, $kolomhead++, "Mgodown");
		xlsWriteLabel($tablehead, $kolomhead++, "Product");
		xlsWriteLabel($tablehead, $kolomhead++, "Stock");
		xlsWriteLabel($tablehead, $kolomhead++, "Price");
		xlsWriteLabel($tablehead, $kolomhead++, "Ordered On");
		xlsWriteLabel($tablehead, $kolomhead++, "Received Date");
		xlsWriteLabel($tablehead, $kolomhead++, "Status");

		foreach ($this->Input_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteNumber($tablebody, $kolombody++, $data->mid);
			xlsWriteNumber($tablebody, $kolombody++, $data->mgodown);
			xlsWriteLabel($tablebody, $kolombody++, $data->product);
			xlsWriteNumber($tablebody, $kolombody++, $data->stock);
			xlsWriteNumber($tablebody, $kolombody++, $data->price);
			xlsWriteLabel($tablebody, $kolombody++, $data->ordered_on);
			xlsWriteLabel($tablebody, $kolombody++, $data->received_date);
			xlsWriteNumber($tablebody, $kolombody++, $data->status);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

}

/* End of file Input.php */
/* Location: ./application/controllers/Input.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:07:34 */
/* http://harviacode.com */