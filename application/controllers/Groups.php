<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Groups extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Groups_model');
        $this->load->library('form_validation');        
	    $this->load->library('datatables');
        if (!$this->ion_auth->logged_in())
        {
            // redirect them to the login page
            redirect('auth/login', 'refresh');
        }
    }

    public function index()
    {
        if ($this->ion_auth->is_admin()) 
        {
            $this->data['module']="groups/groups_list";
            $this->load->view('layout/datatable', $this->data);
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
        //$this->load->view('groups/groups_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Groups_model->json();
    }

    public function read($id) 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $row = $this->Groups_model->get_by_id($id);
            if ($row) {
                $data = array(
                    'id' => $row->id,
                    'name' => $row->name,
                    'description' => $row->description,
                );
                $this->load->view('groups/groups_read', $data);
            } else {
                $this->session->set_flashdata('message', 'Record Not Found');
                redirect(site_url('groups'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
            
    }

    public function create() 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $data = array(
                'button' => 'Create',
                'action' => site_url('groups/create_action'),
                'id' => set_value('id'),
                'name' => set_value('name'),
                'description' => set_value('description'),
            );
            $this->load->view('groups/groups_form', $data);
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
    }
    
    public function create_action() 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $this->_rules();
            if ($this->form_validation->run() == FALSE) {
                $this->create();
            } else {
                $data = array(
                    'name' => $this->input->post('name',TRUE),
                    'description' => $this->input->post('description',TRUE),
                );

                $this->Groups_model->insert($data);
                $this->session->set_flashdata('message', 'Create Record Success');
                redirect(site_url('groups'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
    }
    
    public function update($id) 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $row = $this->Groups_model->get_by_id($id);

            if ($row) {
                $data = array(
                    'button' => 'Update',
                    'action' => site_url('groups/update_action'),
                    'id' => set_value('id', $row->id),
                    'name' => set_value('name', $row->name),
                    'description' => set_value('description', $row->description),
                );
                $this->load->view('groups/groups_form', $data);
            } else {
                $this->session->set_flashdata('message', 'Record Not Found');
                redirect(site_url('groups'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }

            
    }
    
    public function update_action() 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $this->_rules();

            if ($this->form_validation->run() == FALSE) {
                $this->update($this->input->post('id', TRUE));
            } else {
                $data = array(
                    'name' => $this->input->post('name',TRUE),
                    'description' => $this->input->post('description',TRUE),
                );

                $this->Groups_model->update($this->input->post('id', TRUE), $data);
                $this->session->set_flashdata('message', 'Update Record Success');
                redirect(site_url('groups'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
    }
    
    public function delete($id) 
    {
        if ($this->ion_auth->is_admin()) 
        {
            $row = $this->Groups_model->get_by_id($id);

            if ($row) {
                $this->Groups_model->delete($id);
                $this->session->set_flashdata('message', 'Delete Record Success');
                redirect(site_url('groups'));
            } else {
                $this->session->set_flashdata('message', 'Record Not Found');
                redirect(site_url('groups'));
            }
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
            
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('name', 'name', 'trim|required');
	$this->form_validation->set_rules('description', 'description', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        if ($this->ion_auth->is_admin()) 
        {
            $this->load->helper('exportexcel');
            $namaFile = "groups.xls";
            $judul = "groups";
            $tablehead = 0;
            $tablebody = 1;
            $nourut = 1;
            //penulisan header
            header("Pragma: public");
            header("Expires: 0");
            header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
            header("Content-Type: application/force-download");
            header("Content-Type: application/octet-stream");
            header("Content-Type: application/download");
            header("Content-Disposition: attachment;filename=" . $namaFile . "");
            header("Content-Transfer-Encoding: binary ");

            xlsBOF();

            $kolomhead = 0;
            xlsWriteLabel($tablehead, $kolomhead++, "No");
            xlsWriteLabel($tablehead, $kolomhead++, "Name");
            xlsWriteLabel($tablehead, $kolomhead++, "Description");

            foreach ($this->Groups_model->get_all() as $data) {
                $kolombody = 0;

                //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
                xlsWriteNumber($tablebody, $kolombody++, $nourut);
                xlsWriteLabel($tablebody, $kolombody++, $data->name);
                xlsWriteLabel($tablebody, $kolombody++, $data->description);

                $tablebody++;
                $nourut++;
            }

            xlsEOF();
            exit();
        }else{
            $this->data['module']="layout/404";
            $this->load->view('layout/datatable', $this->data);
        }
    }
}

/* End of file Groups.php */
/* Location: ./application/controllers/Groups.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-06-15 10:57:40 */
/* http://harviacode.com */