<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Manufacture extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Manufacture_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		//$this->load->view('manufacture/manufacture_list');
		$this->data['module'] = 'manufacture/manufacture_list';
		$this->load->view('layout/datatable', $this->data);
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Manufacture_model->json();
	}

	public function read($id) {
		$row = $this->Manufacture_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'          => $row->id,
				'mname'       => $row->mname,
				'pcat'        => $row->pcat,
				'clogo'       => $row->clogo,
				'address'     => $row->address,
				'contact_add' => $row->contact_add,
				'email'       => $row->email,
				'created_on'  => $row->created_on,
				'created_by'  => $row->created_by,
				'modified_on' => $row->modified_on,
				'modified_by' => $row->modified_by,
				'status'      => $row->status,
			);
			$this->data['module'] = 'manufacture/manufacture_read';
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('manufacture'));
		}
	}

	public function create() {
		$this->data = array(
			'button'      => 'Create',
			'action'      => site_url('manufacture/create_action'),
			'id'          => set_value('id'),
			'mname'       => set_value('mname'),
			'pcat'        => set_value('pcat'),
			'clogo'       => set_value('clogo'),
			'address'     => set_value('address'),
			'contact_add' => set_value('contact_add'),
			'email'       => set_value('email'),
			'created_on'  => set_value('created_on'),
			'created_by'  => set_value('created_by'),
			'modified_on' => set_value('modified_on'),
			'modified_by' => set_value('modified_by'),
			'status'      => set_value('status'),
		);
		$this->data['123']=$this->Manufacture_model->get_123();
		$this->data['module'] = 'manufacture/manufacture_form';
		$this->load->view('layout/form', $this->data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'mname'       => $this->input->post('mname', TRUE),
				'pcat'        => $this->input->post('pcat', TRUE),
				'clogo'       => $this->input->post('clogo', TRUE),
				'address'     => $this->input->post('address', TRUE),
				'contact_add' => $this->input->post('contact_add', TRUE),
				'email'       => $this->input->post('email', TRUE),
				'created_on'  => date('Y-m-d H:i:s'),
				'created_by'  => $this->session->userdata('user_id'),
				'status'      => $this->input->post('status', TRUE),
			);

			$this->Manufacture_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('manufacture'));
		}
	}

	public function update($id) {
		$row = $this->Manufacture_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button'      => 'Update',
				'action'      => site_url('manufacture/update_action'),
				'id'          => set_value('id', $row->id),
				'mname'       => set_value('mname', $row->mname),
				'pcat'        => set_value('pcat', $row->pcat),
				'clogo'       => set_value('clogo', $row->clogo),
				'address'     => set_value('address', $row->address),
				'contact_add' => set_value('contact_add', $row->contact_add),
				'email'       => set_value('email', $row->email),
				'created_on'  => set_value('created_on', $row->created_on),
				'created_by'  => set_value('created_by', $row->created_by),
				'modified_on' => set_value('modified_on', $row->modified_on),
				'modified_by' => set_value('modified_by', $row->modified_by),
				'status'      => set_value('status', $row->status),
			);
			$this->data['123']=$this->Manufcture_model->get_123();
			$this->data['module'] = 'manufacture/manufacture_form';
			$this->load->view('layout/form', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('manufacture'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'mname'       => $this->input->post('mname', TRUE),
				'pcat'        => $this->input->post('pcat', TRUE),
				'clogo'       => $this->input->post('clogo', TRUE),
				'address'     => $this->input->post('address', TRUE),
				'contact_add' => $this->input->post('contact_add', TRUE),
				'email'       => $this->input->post('email', TRUE),
				'modified_on' => date('Y-m-d H:i:s'),
				'modified_by' => $this->session->userdata('user_id'),
				'status'      => $this->input->post('status', TRUE),
			);

			$this->Manufacture_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('manufacture'));
		}
	}

	public function delete($id) {
		$row = $this->Manufacture_model->get_by_id($id);

		if ($row) {
			$this->Manufacture_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('manufacture'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('manufacture'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('mname', 'mname', 'trim|required');
		$this->form_validation->set_rules('pcat', 'pcat', 'trim|required');
		$this->form_validation->set_rules('clogo', 'clogo', 'trim|required');
		$this->form_validation->set_rules('address', 'address', 'trim|required');
		$this->form_validation->set_rules('contact_add', 'contact add', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required');
		$this->form_validation->set_rules('created_on', 'created on', 'trim');
		$this->form_validation->set_rules('created_by', 'created by', 'trim');
		$this->form_validation->set_rules('modified_on', 'modified on', 'trim');
		$this->form_validation->set_rules('modified_by', 'modified by', 'trim');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "manufacture.xls";
		$judul     = "manufacture";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Mname");
		xlsWriteLabel($tablehead, $kolomhead++, "Pcat");
		xlsWriteLabel($tablehead, $kolomhead++, "Clogo");
		xlsWriteLabel($tablehead, $kolomhead++, "Address");
		xlsWriteLabel($tablehead, $kolomhead++, "Contact Add");
		xlsWriteLabel($tablehead, $kolomhead++, "Email");
		xlsWriteLabel($tablehead, $kolomhead++, "Created On");
		xlsWriteLabel($tablehead, $kolomhead++, "Created By");
		xlsWriteLabel($tablehead, $kolomhead++, "Modified On");
		xlsWriteLabel($tablehead, $kolomhead++, "Modified By");
		xlsWriteLabel($tablehead, $kolomhead++, "Status");

		foreach ($this->Manufacture_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->mname);
			xlsWriteNumber($tablebody, $kolombody++, $data->pcat);
			xlsWriteLabel($tablebody, $kolombody++, $data->clogo);
			xlsWriteLabel($tablebody, $kolombody++, $data->address);
			xlsWriteLabel($tablebody, $kolombody++, $data->contact_add);
			xlsWriteLabel($tablebody, $kolombody++, $data->email);
			xlsWriteLabel($tablebody, $kolombody++, $data->created_on);
			xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
			xlsWriteLabel($tablebody, $kolombody++, $data->modified_on);
			xlsWriteNumber($tablebody, $kolombody++, $data->modified_by);
			xlsWriteNumber($tablebody, $kolombody++, $data->status);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

}

/* End of file Manufacture.php */
/* Location: ./application/controllers/Manufacture.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:09:28 */
/* http://harviacode.com */