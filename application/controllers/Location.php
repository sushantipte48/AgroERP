<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Location extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('M_location_model');
        $this->load->library('form_validation');        
	    $this->load->library('datatables');
    }

    public function index()
    {
        $data['module']="location/m_location_list";
        $this->load->view('layout/datatable',$data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->M_location_model->json();
    }

    public function get_like_district()
    {
        $search = $_GET['q'];
        $limit = $_GET['page_limit'];
        echo $this->M_location_model->get_like_district( $search,$limit);
    }

    public function get_district_by_id($id){
      header('Content-Type: application/json');
      $result = $this->M_location_model->get_district_by_id($id);
      echo json_encode(Array('Id'=>$result->DistrictId,'MarketName'=>$result->District));
      //echo json_encode($result);
    }

    public function read($id) 
    {
        $row = $this->M_location_model->get_by_id($id);
        if ($row) {
            $data = array(
		'Id' => $row->Id,
		'Code' => $row->Code,
		'MarketName1' => $row->MarketName,
		'Country' => $row->Country,
		'State' => $row->State,
		'District' => $row->District,
		'CreatedOn' => $row->CreatedOn,
		'CreatedBy' => $row->CreatedBy,
		'ModifyOn' => $row->ModifyOn,
		'ModifyBy' => $row->ModifyBy,
		'Status' => $row->Status,
	    );
            $this->load->view('location/m_location_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('location'));
        }
    }

    public function create() 
    {
        $data = array(
        'button' => 'Create',
        'action' => site_url('location/create_action'),
	    'Id' => set_value('Id'),
	    'Code' => set_value('Code'),
	    'MarketName1' => set_value('MarketName1'),
        'Pin' => set_value('Pin'),
	    'Country' => set_value('Country'),
	    'State' => set_value('State'),
	    'Status' => set_value('Status'),
	);
        $data['title']="CREATE";
        $data['module']="location/m_location_form";
        $this->load->view('layout/form',$data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
    		'Name' => $this->input->post('MarketName1',TRUE),
    		'ParentId' => $this->input->post('District',TRUE),
            'LocationType'=>'SUBDISTRICT',
            'Pin'=> $this->input->post('Pin',TRUE),
    		'CreatedOn' => date('Y-m-d H:i:s'),
    		'CreatedBy' => $this->session->userdata('user_id'),
    		'Status' => $this->input->post('Status',TRUE),
    	    );

            $this->M_location_model->insert($data);
            $this->session->set_flashdata('message', 'Location Successfully Added.');
            redirect(site_url('location'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->M_location_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('location/update_action'),
		'Id' => set_value('Id', $row->Id),
		'MarketName1' => set_value('MarketName1', $row->Name),
        'Pin' => set_value('Pin', $row->Pin),
		'District' => set_value('District', $row->ParentId),
		'Status' => set_value('Status', $row->Status),
	    );
            $data['title']="UPDATE";
            $data['module']="location/m_location_form";
            $this->load->view('layout/form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('location'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('Id', TRUE));
        } else {
            $data = array(
		'Code' => $this->input->post('Code',TRUE),
		'MarketName' => $this->input->post('MarketName1',TRUE),
		'Country' => $this->input->post('Country',TRUE),
		'State' => $this->input->post('State',TRUE),
		'District' => $this->input->post('District',TRUE),
		'CreatedOn' => $this->input->post('CreatedOn',TRUE),
		'CreatedBy' => $this->input->post('CreatedBy',TRUE),
		'ModifyOn' => $this->input->post('ModifyOn',TRUE),
		'ModifyBy' => $this->input->post('ModifyBy',TRUE),
		'Status' => $this->input->post('Status',TRUE),
	    );

            $this->M_location_model->update($this->input->post('Id', TRUE), $data);
            $this->session->set_flashdata('message', 'Location Successfully Updated.');
            redirect(site_url('location'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->M_location_model->get_by_id($id);

        if ($row) {
            $this->M_location_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('location'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('location'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('MarketName1', 'market name', 'trim|required');
	$this->form_validation->set_rules('Pin', 'pin', 'trim|required');
	$this->form_validation->set_rules('District', 'district', 'trim|required|callback_district');
	$this->form_validation->set_rules('Status', 'status', 'trim|required');

	$this->form_validation->set_rules('Id', 'Id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function district()
    {

        $MarketName = $this->input->post('MarketName1');
        $District = $this->input->post('District');

        $check = $this->db->get_where('geo_locations', array('id' => $MarketName, 'ParentId' => $District), 1);

        if ($check->num_rows() > 0) {

            $this->form_validation->set_message('district', 'This Marketplace is already entered');
            return FALSE;
        }

        return TRUE;
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "m_location.xls";
        $judul = "m_location";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Code");
	xlsWriteLabel($tablehead, $kolomhead++, "MarketName");
	xlsWriteLabel($tablehead, $kolomhead++, "Country");
	xlsWriteLabel($tablehead, $kolomhead++, "State");
	xlsWriteLabel($tablehead, $kolomhead++, "District");
	xlsWriteLabel($tablehead, $kolomhead++, "CreatedOn");
	xlsWriteLabel($tablehead, $kolomhead++, "CreatedBy");
	xlsWriteLabel($tablehead, $kolomhead++, "ModifyOn");
	xlsWriteLabel($tablehead, $kolomhead++, "ModifyBy");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->M_location_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->Code);
	    xlsWriteLabel($tablebody, $kolombody++, $data->MarketName);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Country);
	    xlsWriteNumber($tablebody, $kolombody++, $data->State);
	    xlsWriteNumber($tablebody, $kolombody++, $data->District);
	    xlsWriteLabel($tablebody, $kolombody++, $data->CreatedOn);
	    xlsWriteNumber($tablebody, $kolombody++, $data->CreatedBy);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ModifyOn);
	    xlsWriteNumber($tablebody, $kolombody++, $data->ModifyBy);
	    xlsWriteNumber($tablebody, $kolombody++, $data->Status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Location.php */
/* Location: ./application/controllers/Location.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-06-24 16:07:09 */
/* http://harviacode.com */