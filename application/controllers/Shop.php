<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Shop extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Shop_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		$this->data['module'] = 'shop/shop_list';
		$this->load->view('layout/datatable', $this->data);
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Shop_model->json();
	}

	public function read($id) {
		$row = $this->Shop_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'          => $row->id,
				'sname'       => $row->sname,
				'sadd'        => $row->sadd,
				'contact'     => $row->contact,
				'email'       => $row->email,
				'created_on'  => $row->created_on,
				'created_by'  => $row->created_by,
				'modified_on' => $row->modified_on,
				'modified_by' => $row->modified_by,
				'status'      => $row->status,
			);
			$this->data['module'] = 'shop/shop_read';
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('shop'));
		}
	}

	public function create() {
		$this->data = array(
			'button'      => 'Create',
			'action'      => site_url('shop/create_action'),
			'id'          => set_value('id'),
			'sname'       => set_value('sname'),
			'sadd'        => set_value('sadd'),
			'contact'     => set_value('contact'),
			'email'       => set_value('email'),
			'created_on'  => set_value('created_on'),
			'created_by'  => set_value('created_by'),
			'modified_on' => set_value('modified_on'),
			'modified_by' => set_value('modified_by'),
			'status'      => set_value('status'),
		);
		$this->data['module'] = 'shop/shop_form';
		$this->load->view('layout/form', $this->data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'sname'      => $this->input->post('sname', TRUE),
				'sadd'       => $this->input->post('sadd', TRUE),
				'contact'    => $this->input->post('contact', TRUE),
				'email'      => $this->input->post('email', TRUE),
				'created_on' => date('Y-m-d H:i:s'),
				'created_by' => $this->session->userdata('user_id'),
				'status'     => $this->input->post('status', TRUE),
			);

			$this->Shop_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('shop'));
		}
	}

	public function update($id) {
		$row = $this->Shop_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button'      => 'Update',
				'action'      => site_url('shop/update_action'),
				'id'          => set_value('id', $row->id),
				'sname'       => set_value('sname', $row->sname),
				'sadd'        => set_value('sadd', $row->sadd),
				'contact'     => set_value('contact', $row->contact),
				'email'       => set_value('email', $row->email),
				'created_on'  => set_value('created_on', $row->created_on),
				'created_by'  => set_value('created_by', $row->created_by),
				'modified_on' => set_value('modified_on', $row->modified_on),
				'modified_by' => set_value('modified_by', $row->modified_by),
				'status'      => set_value('status', $row->status),
			);
			$this->data['module'] = 'shop/shop_form';
			$this->load->view('layout/form', $this->data);
			//$this->load->view('shop/shop_form', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('shop'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'sname'       => $this->input->post('sname', TRUE),
				'sadd'        => $this->input->post('sadd', TRUE),
				'contact'     => $this->input->post('contact', TRUE),
				'email'       => $this->input->post('email', TRUE),
				'modified_on' => date('Y-m-d H:i:s'),
				'modified_by' => $this->session->userdata('user_id'),
				'status'      => $this->input->post('status', TRUE),
			);

			$this->Shop_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('shop'));
		}
	}

	public function delete($id) {
		$row = $this->Shop_model->get_by_id($id);

		if ($row) {
			$this->Shop_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('shop'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('shop'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('sname', 'sname', 'trim|required');
		$this->form_validation->set_rules('sadd', 'sadd', 'trim|required');
		$this->form_validation->set_rules('contact', 'contact', 'trim|required');
		$this->form_validation->set_rules('email', 'email', 'trim|required');
		$this->form_validation->set_rules('created_on', 'created on', 'trim');
		$this->form_validation->set_rules('created_by', 'created by', 'trim');
		$this->form_validation->set_rules('modified_on', 'modified on', 'trim');
		$this->form_validation->set_rules('modified_by', 'modified by', 'trim');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "shop.xls";
		$judul     = "shop";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Sname");
		xlsWriteLabel($tablehead, $kolomhead++, "Sadd");
		xlsWriteLabel($tablehead, $kolomhead++, "Contact");
		xlsWriteLabel($tablehead, $kolomhead++, "Email");
		xlsWriteLabel($tablehead, $kolomhead++, "Created On");
		xlsWriteLabel($tablehead, $kolomhead++, "Created By");
		xlsWriteLabel($tablehead, $kolomhead++, "Modified On");
		xlsWriteLabel($tablehead, $kolomhead++, "Modified By");
		xlsWriteLabel($tablehead, $kolomhead++, "Status");

		foreach ($this->Shop_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteLabel($tablebody, $kolombody++, $data->sname);
			xlsWriteLabel($tablebody, $kolombody++, $data->sadd);
			xlsWriteNumber($tablebody, $kolombody++, $data->contact);
			xlsWriteLabel($tablebody, $kolombody++, $data->email);
			xlsWriteLabel($tablebody, $kolombody++, $data->created_on);
			xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
			xlsWriteLabel($tablebody, $kolombody++, $data->modified_on);
			xlsWriteNumber($tablebody, $kolombody++, $data->modified_by);
			xlsWriteNumber($tablebody, $kolombody++, $data->status);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

}

/* End of file Shop.php */
/* Location: ./application/controllers/Shop.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:08:04 */
/* http://harviacode.com */