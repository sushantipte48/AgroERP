<?php

if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Offer extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('Offer_model');
		$this->load->library('form_validation');
		$this->load->library('datatables');
	}

	public function index() {
		$this->data['module'] = 'offer/offer_list';
		$this->load->view('layout/datatable', $this->data);
	}

	public function json() {
		header('Content-Type: application/json');
		echo $this->Offer_model->json();
	}

	public function read($id) {
		$row = $this->Offer_model->get_by_id($id);
		if ($row) {
			$this->data = array(
				'id'          => $row->id,
				'pid'         => $row->pid,
				'discount'    => $row->discount,
				'disc_type'   => $row->disc_type,
				'from'        => $row->from,
				'to'          => $row->to,
				'created_by'  => $row->created_by,
				'created_on'  => $row->created_on,
				'modified_by' => $row->modified_by,
				'modified_on' => $row->modified_on,
				'status'      => $row->status,
			);
			$this->data['module'] = 'offer/offer_read';
			$this->load->view('layout/datatable', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('offer'));
		}
	}

	public function create() {
		$this->data = array(
			'button'      => 'Create',
			'action'      => site_url('offer/create_action'),
			'id'          => set_value('id'),
			'pid'         => set_value('pid'),
			'discount'    => set_value('discount'),
			'disc_type'   => set_value('disc_type'),
			'from'        => set_value('from'),
			'to'          => set_value('to'),
			'created_by'  => set_value('created_by'),
			'created_on'  => set_value('created_on'),
			'modified_by' => set_value('modified_by'),
			'modified_on' => set_value('modified_on'),
			'status'      => set_value('status'),
		);
		if($this->ion_auth->is_admin())
		{
			$this->data['offer']=$this->Offer_model->get_offer();
		}
		if($this->ion_auth->is_company())
		{
			$this->data['offer']=$this->Offer_model->get_offer();
		}
		$this->data['module'] = 'offer/offer_form';
		$this->load->view('layout/form', $this->data);
	}

	public function create_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->create();
		} else {
			$data = array(
				'pid'        => $this->input->post('pid', TRUE),
				'discount'   => $this->input->post('discount', TRUE),
				'disc_type'  => $this->input->post('disc_type', TRUE),
				'from'       => $this->input->post('from', TRUE),
				'to'         => $this->input->post('to', TRUE),
				'created_by' => $this->session->userdata('user_id'),
				'created_on' => date('Y-m-d H:i:s'),
				'status'     => $this->input->post('status', TRUE),
			);

			$this->Offer_model->insert($data);
			$this->session->set_flashdata('message', 'Create Record Success');
			redirect(site_url('offer'));
		}
	}

	public function update($id) {
		$row = $this->Offer_model->get_by_id($id);

		if ($row) {
			$this->data = array(
				'button'      => 'Update',
				'action'      => site_url('offer/update_action'),
				'id'          => set_value('id', $row->id),
				'pid'         => set_value('pid', $row->pid),
				'discount'    => set_value('discount', $row->discount),
				'disc_type'   => set_value('disc_type', $row->disc_type),
				'from'        => set_value('from', $row->from),
				'to'          => set_value('to', $row->to),
				'created_by'  => set_value('created_by', $row->created_by),
				'created_on'  => set_value('created_on', $row->created_on),
				'modified_by' => set_value('modified_by', $row->modified_by),
				'modified_on' => set_value('modified_on', $row->modified_on),
				'status'      => set_value('status', $row->status),
			);
			$this->data['offer']=$this->Offer_model->get_offer();
			$this->data['module'] = 'offer/offer_form';
			$this->load->view('layout/form', $this->data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('offer'));
		}
	}

	public function update_action() {
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'pid'         => $this->input->post('pid', TRUE),
				'discount'    => $this->input->post('discount', TRUE),
				'disc_type'   => $this->input->post('disc_type', TRUE),
				'from'        => $this->input->post('from', TRUE),
				'to'          => $this->input->post('to', TRUE),
				'modified_by' => $this->session->userdata('user_id'),
				'modified_on' => date('Y-m-d H:i:s'),
				'status'      => $this->input->post('status', TRUE),
			);

			$this->Offer_model->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('offer'));
		}
	}

	public function delete($id) {
		$row = $this->Offer_model->get_by_id($id);

		if ($row) {
			$this->Offer_model->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('offer'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('offer'));
		}
	}

	public function _rules() {
		$this->form_validation->set_rules('pid', 'pid', 'trim|required');
		$this->form_validation->set_rules('discount', 'discount', 'trim|required');
		$this->form_validation->set_rules('disc_type', 'disc type', 'trim|required');
		$this->form_validation->set_rules('from', 'from', 'trim|required');
		$this->form_validation->set_rules('to', 'to', 'trim|required');
		$this->form_validation->set_rules('created_by', 'created by', 'trim');
		$this->form_validation->set_rules('created_on', 'created on', 'trim');
		$this->form_validation->set_rules('modified_by', 'modified by', 'trim');
		$this->form_validation->set_rules('modified_on', 'modified on', 'trim');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('id', 'id', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
	}

	public function excel() {
		$this->load->helper('exportexcel');
		$namaFile  = "offer.xls";
		$judul     = "offer";
		$tablehead = 0;
		$tablebody = 1;
		$nourut    = 1;
		//penulisan header
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
		header("Content-Type: application/force-download");
		header("Content-Type: application/octet-stream");
		header("Content-Type: application/download");
		header("Content-Disposition: attachment;filename=".$namaFile."");
		header("Content-Transfer-Encoding: binary ");

		xlsBOF();

		$kolomhead = 0;
		xlsWriteLabel($tablehead, $kolomhead++, "No");
		xlsWriteLabel($tablehead, $kolomhead++, "Pid");
		xlsWriteLabel($tablehead, $kolomhead++, "Discount");
		xlsWriteLabel($tablehead, $kolomhead++, "Disc Type");
		xlsWriteLabel($tablehead, $kolomhead++, "From");
		xlsWriteLabel($tablehead, $kolomhead++, "To");
		xlsWriteLabel($tablehead, $kolomhead++, "Created By");
		xlsWriteLabel($tablehead, $kolomhead++, "Created On");
		xlsWriteLabel($tablehead, $kolomhead++, "Modified By");
		xlsWriteLabel($tablehead, $kolomhead++, "Modified On");
		xlsWriteLabel($tablehead, $kolomhead++, "Status");

		foreach ($this->Offer_model->get_all() as $data) {
			$kolombody = 0;

			//ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
			xlsWriteNumber($tablebody, $kolombody++, $nourut);
			xlsWriteNumber($tablebody, $kolombody++, $data->pid);
			xlsWriteNumber($tablebody, $kolombody++, $data->discount);
			xlsWriteNumber($tablebody, $kolombody++, $data->disc_type);
			xlsWriteLabel($tablebody, $kolombody++, $data->from);
			xlsWriteLabel($tablebody, $kolombody++, $data->to);
			xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
			xlsWriteLabel($tablebody, $kolombody++, $data->created_on);
			xlsWriteNumber($tablebody, $kolombody++, $data->modified_by);
			xlsWriteLabel($tablebody, $kolombody++, $data->modified_on);
			xlsWriteNumber($tablebody, $kolombody++, $data->status);

			$tablebody++;
			$nourut++;
		}

		xlsEOF();
		exit();
	}

}

/* End of file Offer.php */
/* Location: ./application/controllers/Offer.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-08-23 11:08:18 */
/* http://harviacode.com */