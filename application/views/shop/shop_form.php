<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
             <div class="alert alert-info display-show">
                <button class="close" data-close="alert"></button>
                <span><?php echo $message;?></span>
             </div>
    <?php }?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>SHOP <?php echo $button;?></h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Shop Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Shop Create
                </li>
             </ul>
            <hr>
        <h2 style="margin-top:0px">Shop <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
	    <!-- <div class="form-group">
            <label for="varchar">Sname <?php echo form_error('sname') ?></label>
            <input type="text" class="form-control" name="sname" id="sname" placeholder="Sname" value="<?php echo $sname; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Sadd <?php echo form_error('sadd') ?></label>
            <input type="text" class="form-control" name="sadd" id="sadd" placeholder="Sadd" value="<?php echo $sadd; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Contact <?php echo form_error('contact') ?></label>
            <input type="text" class="form-control" name="contact" id="contact" placeholder="Contact" value="<?php echo $contact; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Email <?php echo form_error('email') ?></label>
            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
        </div> -->
	    <!-- <div class="form-group">
            <label for="date">Created On <?php echo form_error('created_on') ?></label>
            <input type="text" class="form-control" name="created_on" id="created_on" placeholder="Created On" value="<?php echo $created_on; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Modified On <?php echo form_error('modified_on') ?></label>
            <input type="text" class="form-control" name="modified_on" id="modified_on" placeholder="Modified On" value="<?php echo $modified_on; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Modified By <?php echo form_error('modified_by') ?></label>
            <input type="text" class="form-control" name="modified_by" id="modified_by" placeholder="Modified By" value="<?php echo $modified_by; ?>" />
        </div> -->
	    <!-- <div class="form-group">
            <label for="int">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div> -->
         <div class="form-group">
            <label class="control-label col-md-3">Shop Name <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="sname" id="sname" value="<?php echo $sname;?>" />
               <span><?php echo form_error('sname')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Shop Address <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="sadd" id="sadd" value="<?php echo $sadd;?>" />
               <span><?php echo form_error('sadd')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Contact <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="contact" id="contact" value="<?php echo $contact;?>" />
               <span><?php echo form_error('contact')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Email <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="email" id="email" value="<?php echo $email;?>" />
               <span><?php echo form_error('email')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Status <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <select name="status" id="status" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <option value="0">Deactive</option>
                    <option value="1">Active</option>
                </select>
               <span><?php echo form_error('status')?></span>
            </div>
         </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('shop') ?>" class="btn btn-default">Cancel</a>
        </form></div></div></div></div></div>