<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Shop Read</h2>
        <table class="table">
	    <tr><td>Sname</td><td><?php echo $sname; ?></td></tr>
	    <tr><td>Sadd</td><td><?php echo $sadd; ?></td></tr>
	    <tr><td>Contact</td><td><?php echo $contact; ?></td></tr>
	    <tr><td>Email</td><td><?php echo $email; ?></td></tr>
	    <tr><td>Created On</td><td><?php echo $created_on; ?></td></tr>
	    <tr><td>Created By</td><td><?php echo $created_by; ?></td></tr>
	    <tr><td>Modified On</td><td><?php echo $modified_on; ?></td></tr>
	    <tr><td>Modified By</td><td><?php echo $modified_by; ?></td></tr>
	    <tr><td>Status</td><td><?php echo $status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('shop') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>