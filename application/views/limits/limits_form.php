<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Limits <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Uri <?php echo form_error('uri') ?></label>
            <input type="text" class="form-control" name="uri" id="uri" placeholder="Uri" value="<?php echo $uri; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Count <?php echo form_error('count') ?></label>
            <input type="text" class="form-control" name="count" id="count" placeholder="Count" value="<?php echo $count; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Hour Started <?php echo form_error('hour_started') ?></label>
            <input type="text" class="form-control" name="hour_started" id="hour_started" placeholder="Hour Started" value="<?php echo $hour_started; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Api Key <?php echo form_error('api_key') ?></label>
            <input type="text" class="form-control" name="api_key" id="api_key" placeholder="Api Key" value="<?php echo $api_key; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('limits') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>