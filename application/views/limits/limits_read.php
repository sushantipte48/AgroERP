<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Limits Read</h2>
        <table class="table">
	    <tr><td>Uri</td><td><?php echo $uri; ?></td></tr>
	    <tr><td>Count</td><td><?php echo $count; ?></td></tr>
	    <tr><td>Hour Started</td><td><?php echo $hour_started; ?></td></tr>
	    <tr><td>Api Key</td><td><?php echo $api_key; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('limits') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>