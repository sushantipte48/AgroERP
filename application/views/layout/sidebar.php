<div class="page-sidebar-wrapper">
   <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
   <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
   <div class="page-sidebar navbar-collapse collapse">
      <ul class="page-sidebar-menu page-sidebar-menu-hover-submenu1" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
         <!-- <li class="heading">
            <h3>HOME</h3>
         </li>
         <li class="active">
            <a href="<?php echo base_url('auth');?>">
            <i class="fa fa-tachometer" aria-hidden="true"></i>
            <span class="title">DASHBOARD</span>
            </a>
         </li>-->
<?php if ($this->ion_auth->is_admin()) {?>
							<li>
            <a href="javascript:;">
            <i class="fa fa-lock" aria-hidden="true"></i>
            <span class="title">ADMIN</span>
            <span class="arrow "></span>
            </a>
            <ul class="sub-menu">
               <li>
                  <a href="<?php echo base_url('auth');?>">
                  <i class="fa fa-users" aria-hidden="true"></i>
                  MANAGE USERS</a>
               </li>
            </ul>
         </li>
					         <li>
					            <a href="<?php echo base_url('product');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">PRODUCTS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('shop');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">SHOPS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('offer');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">OFFERS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('manufacture');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">MANUFACTURERS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('manugodown');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">MANUFACTURER GODOWN</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('agrogodown');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">OUR GODOWN</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('input');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">ORDERS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('output');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">DELIVERY</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('stock');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">STOCK</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('review');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">REVIEW</span>
					           </a>
					         </li>
	<?php }?>
         <?php if ($this->ion_auth->is_customer()) {?>
					         <li>
					            <a href="<?php echo base_url('product');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">PRODUCTS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('offer');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">OFFERS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('input');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">ORDERS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('output');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">DELIVERY</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('review');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">REVIEW</span>
					           </a>
					         </li>
	<?php }?>
         <?php if ($this->ion_auth->is_company()) {?>
					         <li>
					            <a href="<?php echo base_url('product');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">PRODUCTS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('offer');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">OFFERS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('manugodown');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">MANUFACTURER GODOWN</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('input');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">ORDERS</span>
					           </a>
					         </li>
					         <li>
					            <a href="<?php echo base_url('stock');?>">
					            <i class="fa fa-check-square-o" aria-hidden="true"></i>
					            <span class="title">STOCK</span>
					           </a>
					         </li>
	<?php }?>
      </ul>
      <!-- END SIDEBAR MENU -->
   </div>
</div>