<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
             <div class="alert alert-info display-show">
                <button class="close" data-close="alert"></button>
                <span><?php echo $message;?></span>
             </div>
    <?php }?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>MANUFACTURE <?php echo $button;?></h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Manufacture Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Manufacture Create
                </li>
             </ul>
            <hr>
        <h2 style="margin-top:0px">Manufacture <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
	    <!-- <div class="form-group">
            <label for="varchar">Mname <?php echo form_error('mname') ?></label>
            <input type="text" class="form-control" name="mname" id="mname" placeholder="Mname" value="<?php echo $mname; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Pcat <?php echo form_error('pcat') ?></label>
            <input type="text" class="form-control" name="pcat" id="pcat" placeholder="Pcat" value="<?php echo $pcat; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Clogo <?php echo form_error('clogo') ?></label>
            <input type="text" class="form-control" name="clogo" id="clogo" placeholder="Clogo" value="<?php echo $clogo; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Address <?php echo form_error('address') ?></label>
            <input type="text" class="form-control" name="address" id="address" placeholder="Address" value="<?php echo $address; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Contact Add <?php echo form_error('contact_add') ?></label>
            <input type="text" class="form-control" name="contact_add" id="contact_add" placeholder="Contact Add" value="<?php echo $contact_add; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Email <?php echo form_error('email') ?></label>
            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo $email; ?>" />
        </div> -->
	    <!--<div class="form-group">
            <label for="int">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div> -->
         <div class="form-group">
            <label class="control-label col-md-3">Manufacture Name <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="mname" id="mname" value="<?php echo $mname;?>" />
               <span><?php echo form_error('mname')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Product Category <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
              <!--  <input type="text" class="form-control" name="pcat" id="pcat" value="<?php echo $pcat;?>" /> -->
              <select name="pcat" id="pcat" type="text" class="form-control" value="">
                    <!--<option value=pcat"">Select</option>-->
                    <option value="">Select</option>
                    <option value="0">Fertilizers</option>
                    <option value="1">Insecticides</option>
                    <option value="2">Seeds</option>
                    <option value="3">Tools</option>
                    </select>
               <span><?php echo form_error('pcat')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Company Logo <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="file" class="form-control" name="clogo" id="clogo" value="<?php echo $clogo;?>" />
               <span><?php echo form_error('clogo')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Address <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="address" id="address" value="<?php echo $address;?>" />
               <span><?php echo form_error('address')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Contact <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="contact_add" id="contact_add" value="<?php echo $contact_add;?>" />
               <span><?php echo form_error('contact_add')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Email <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="email" id="email" value="<?php echo $email;?>" />
               <span><?php echo form_error('email')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Status <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <select name="status" id="status" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <option value="0">Deactive</option>
                    <option value="1">Active</option>
                </select>
               <span><?php echo form_error('status')?></span>
            </div>
         </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('manufacture') ?>" class="btn btn-default">Cancel</a>
	</form></div></div></div></div></div>