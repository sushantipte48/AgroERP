<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Manufacture Read</h2>
        <table class="table">
	    <tr><td>Mname</td><td><?php echo $mname; ?></td></tr>
	    <tr><td>Pcat</td><td><?php echo $pcat; ?></td></tr>
	    <tr><td>Clogo</td><td><?php echo $clogo; ?></td></tr>
	    <tr><td>Address</td><td><?php echo $address; ?></td></tr>
	    <tr><td>Contact Add</td><td><?php echo $contact_add; ?></td></tr>
	    <tr><td>Email</td><td><?php echo $email; ?></td></tr>
	    <tr><td>Created On</td><td><?php echo $created_on; ?></td></tr>
	    <tr><td>Created By</td><td><?php echo $created_by; ?></td></tr>
	    <tr><td>Modified On</td><td><?php echo $modified_on; ?></td></tr>
	    <tr><td>Modified By</td><td><?php echo $modified_by; ?></td></tr>
	    <tr><td>Status</td><td><?php echo $status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('manufacture') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>