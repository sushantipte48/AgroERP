<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
             <div class="alert alert-info display-show">
                <button class="close" data-close="alert"></button>
                <span><?php echo $message;?></span>
             </div>
    <?php }?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>OFFER <?php echo $button;?></h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Offer Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Offer Create
                </li>
             </ul>
            <hr>
        <h2 style="margin-top:0px">Offer <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal">
	    <!-- <div class="form-group">
            <label for="int">Pid <?php echo form_error('pid') ?></label>
            <input type="text" class="form-control" name="pid" id="pid" placeholder="Pid" value="<?php echo $pid; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Discount <?php echo form_error('discount') ?></label>
            <input type="text" class="form-control" name="discount" id="discount" placeholder="Discount" value="<?php echo $discount; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Disc Type <?php echo form_error('disc_type') ?></label>
            <input type="text" class="form-control" name="disc_type" id="disc_type" placeholder="Disc Type" value="<?php echo $disc_type; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">From <?php echo form_error('from') ?></label>
            <input type="text" class="form-control" name="from" id="from" placeholder="From" value="<?php echo $from; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">To <?php echo form_error('to') ?></label>
            <input type="text" class="form-control" name="to" id="to" placeholder="To" value="<?php echo $to; ?>" />
        </div> -->
	    <!-- <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Created On <?php echo form_error('created_on') ?></label>
            <input type="text" class="form-control" name="created_on" id="created_on" placeholder="Created On" value="<?php echo $created_on; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Modified By <?php echo form_error('modified_by') ?></label>
            <input type="text" class="form-control" name="modified_by" id="modified_by" placeholder="Modified By" value="<?php echo $modified_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="date">Modified On <?php echo form_error('modified_on') ?></label>
            <input type="text" class="form-control" name="modified_on" id="modified_on" placeholder="Modified On" value="<?php echo $modified_on; ?>" /> -->
        <!-- </div>
	    <div class="form-group">
            <label for="int">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div> -->
        <div class="form-group">
            <label class="control-label col-md-3">Product ID <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
                <select name="pid" id="pid" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <?php foreach ($offer as $key) {
                        $sel = set_select('pid', $key->id, $pid['value'] == $key->id?True:False);
                        echo "<option value='".$key->id."' ".$sel.">".$key->pname."</option>";
                    }?>
                    </select>
              <!--  <input type="text" class="form-control" name="pid" id="pid" value="<?php echo $pid;?>" /> -->
               <span><?php echo form_error('pid')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Discount <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="discount" id="discount" value="<?php echo $discount;?>" />
               <span><?php echo form_error('discount')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Discount Type <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="disc_type" id="disc_type" value="<?php echo $disc_type;?>" />
               <span><?php echo form_error('disc_type')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">From <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="from" id="from" value="<?php echo $from;?>" />
               <span><?php echo form_error('from')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">To <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="to" id="to" value="<?php echo $to;?>" />
               <span><?php echo form_error('to')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Status <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <!-- <input type="text" class="form-control" name="status" id="status" value="<?php echo $status;?>" /> -->
               <select name="status" id="status" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <option value="0">Deactive</option>
                    <option value="1">Active</option>
                </select>
               <span><?php echo form_error('status')?></span>
            </div>
         </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('offer') ?>" class="btn btn-default">Cancel</a>
    </form></div></div></div></div></div>