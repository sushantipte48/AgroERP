<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Offer Read</h2>
        <table class="table">
	    <tr><td>Pid</td><td><?php echo $pid; ?></td></tr>
	    <tr><td>Discount</td><td><?php echo $discount; ?></td></tr>
	    <tr><td>Disc Type</td><td><?php echo $disc_type; ?></td></tr>
	    <tr><td>From</td><td><?php echo $from; ?></td></tr>
	    <tr><td>To</td><td><?php echo $to; ?></td></tr>
	    <tr><td>Created By</td><td><?php echo $created_by; ?></td></tr>
	    <tr><td>Created On</td><td><?php echo $created_on; ?></td></tr>
	    <tr><td>Modified By</td><td><?php echo $modified_by; ?></td></tr>
	    <tr><td>Modified On</td><td><?php echo $modified_on; ?></td></tr>
	    <tr><td>Status</td><td><?php echo $status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('offer') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>