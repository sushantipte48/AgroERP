<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Product Read</h2>
        <table class="table">
	    <tr><td>Pname</td><td><?php echo $pname; ?></td></tr>
	    <tr><td>Pdesc</td><td><?php echo $pdesc; ?></td></tr>
	    <tr><td>Pcat</td><td><?php echo $pcat; ?></td></tr>
	    <tr><td>Pcost</td><td><?php echo $pcost; ?></td></tr>
	    <tr><td>Pmrp</td><td><?php echo $pmrp; ?></td></tr>
	    <tr><td>Psell</td><td><?php echo $psell; ?></td></tr>
	    <tr><td>Created On</td><td><?php echo $created_on; ?></td></tr>
	    <tr><td>Created By</td><td><?php echo $created_by; ?></td></tr>
	    <tr><td>Modified On</td><td><?php echo $modified_on; ?></td></tr>
	    <tr><td>Modified By</td><td><?php echo $modified_by; ?></td></tr>
	    <tr><td>Status</td><td><?php echo $status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('product') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>