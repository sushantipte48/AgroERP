<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
						 <div class="alert alert-info display-show">
						    <button class="close" data-close="alert"></button>
						    <span><?php echo $message;?></span>
						 </div>
	<?php }?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>Product <?php echo $button;?></h1>
                </div>
                <!-- END PAGE TITLE -->

             </div>
             <!-- END PAGE HEAD -->
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Product Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Product Create
                </li>
             </ul>
            <hr>
        <form action="<?php echo $action;?>" method="post" class="form-horizontal" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-3 text-center">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                     <img  name="photo" id="photo" src="<?php echo base_url()."assets/img/profiles/NoPicAvailable.png";?>" alt="Product Front Image">

                      </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                    </div>
                    <div>
                      <span class="btn default btn-file">
                      <span class="fileinput-new">
                      Select Front Image </span>
                      <span class="fileinput-exists">
                      Change </span>
                      <input type="file" name="front">
                      </span>
                      <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                      Remove </a>
                    </div>
                </div><br>
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                     <img  name="photo" id="photo" src="<?php echo base_url()."assets/img/profiles/NoPicAvailable.png";?>" alt="Product Back Image">

                      </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                    </div>
                    <div>
                      <span class="btn default btn-file">
                      <span class="fileinput-new">
                      Select Back Image </span>
                      <span class="fileinput-exists">
                      Change </span>
                      <input type="file" name="back">
                      </span>
                      <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                      Remove </a>
                    </div>
                </div>
            </div>
        <div class="col-md-9 text-center">
        <div class="form-group">
            <label class="control-label col-md-3">Product Name <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="pname" id="pname" value="<?php echo $pname;?>" />
               <span><?php echo form_error('pname')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Product Description <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="textarea" class="form-control" name="pdesc" id="pdesc" value="<?php echo $pdesc;?>" />
               <span><?php echo form_error('pdesc')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Product Category <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
             <select name="pcat" id="pcat" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <option value="0">Fertilizers</option>
                    <option value="1">Insecticides</option>
                    <option value="2">Seeds</option>
                    <option value="3">Tools</option>
                </select>
               <!-- <input type="text" class="form-control" name="pcat" id="pcat" value="<?php echo $pcat;?>" /> -->
               <span><?php echo form_error('pcat')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Product Cost <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
            <input type="text" class="form-control" name="pcost" id="pcost" value="<?php echo $pcost;?>" />
               <span><?php echo form_error('pcost')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Product MRP <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="pmrp" id="pmrp" value="<?php echo $pmrp;?>" />
               <span><?php echo form_error('pmrp')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Product Sell <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="psell" id="psell" value="<?php echo $psell;?>" />
               <span><?php echo form_error('psell')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Status <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
              <select name="status" id="status" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <option value="0">Deactive</option>
                    <option value="1">Active</option>
                </select>
               <span><?php echo form_error('status')?></span>
            </div>
         </div>
	    <input type="hidden" name="id" value="<?php echo $id;?>" />
	    <button type="submit" class="btn btn-primary"><?php echo $button?></button>
	    <a href="<?php echo site_url('product')?>" class="btn btn-default">Cancel</a>
	</form>
    </body></div></div></div></div></div>