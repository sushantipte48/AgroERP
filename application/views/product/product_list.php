<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
	 <div class="alert alert-info display-show">
	    <button class="close" data-close="alert"></button>
	    <span><?php echo $message;?></span>
	 </div>
	<?php }?>
 <?php if (!empty($this->session->userdata('message'))) {?>
	 <div class="alert alert-info display-show">
	    <button class="close" data-close="alert"></button>
	    <span><?php echo $this->session->userdata('message');?></span>
	 </div>
	<?php }?>
<div class="row">
    <div class="col-md-12">
         <div class="portlet light">
                <div class="portlet-body">

        <div class="row" style="margin-bottom: 10px">
            <div class="col-md-4">
                <!-- BEGIN PAGE HEAD -->
                 <div class="page-head">
                    <!-- BEGIN PAGE TITLE -->
                    <div class="page-title">
                       <h1>PRODUCT MASTER</h1>
                    </div>
                    <!-- END PAGE TITLE -->
                 </div>
                 <!-- END PAGE HEAD -->
                 <!-- BEGIN PAGE BREADCRUMB -->
                 <ul class="page-breadcrumb breadcrumb">
                    <li>
                       <a href="#">Home</a><i class="fa fa-circle"></i>
                    </li>
                    <li>
                       <a href="#">Masters</a><i class="fa fa-circle"></i>
                    </li>
                    <li class="active">
                       Product Master
                    </li>
                 </ul>
                 <!-- END PAGE BREADCRUMB -->
            </div>

            <div class="col-md-4 text-center">
                <div style="margin-top: 4px"  id="message">
<?php echo $this->session->userdata('message') <> ''?$this->session->userdata('message'):'';?>
</div>
            </div>
            <div class="col-md-4 text-right">
<?php echo anchor(site_url('product/create'), 'Create', 'class="btn btn-primary"');?>
		<?php echo anchor(site_url('product/excel'), 'Excel', 'class="btn btn-primary"');?>
	    </div>
        </div>
        <table class="table table-bordered table-striped" id="mytable">
            <thead>
                <tr>
                    <th width="80px">No</th>
		    <th>Product name</th>
		    <th>Description</th>
            <th>Front Pic</th>
            <th>Back Pic</th>
		    <th>Product category</th>
		    <th>Product cost</th>
		    <th>Mrp</th>
		    <th>Selling price</th>
		    <th>Status</th>
		    <th width="200px">Action</th>
                </tr>
            </thead>

        </table>
        <script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js')?>"></script>
        <script src="<?php echo base_url('assets/datatables/jquery.dataTables.js')?>"></script>
        <script src="<?php echo base_url('assets/datatables/dataTables.bootstrap.js')?>"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#mytable").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    oLanguage: {
                        sProcessing: "loading..."
                    },
                    processing: true,
                    serverSide: true,
                    ajax: {"url":  "<?php echo base_url();?>product/json", "type": "POST"},
                    columns: [
                        {
                            "data": "id",
                            "orderable": false
                        },{"data": "pname"},{"data": "pdesc"},{
                            "data":"front_pic",
                            "render": function(data, type, row) {
                                if(data==null)
                                {
                                    return '<img src="<?php echo base_url('assets/img/products/front/NoPicAvailable.png')?>" height="50" width="50" class="text-center"/>';
                                }
                                else
                                {
                                    return '<img src="<?php echo base_url('assets/img/products/front/');?>'+data+'" height="50" width="50" class="text-center"/>';
                                }
                            }
                        },
                        {
                            "data":"back_pic",
                            "render": function(data, type, row) {
                                if(data==null)
                                {
                                    return '<img src="<?php echo base_url('assets/img/products/back/NoPicAvailable.png')?>" height="50" width="50" class="text-center"/>';
                                }
                                else
                                {
                                    return '<img src="<?php echo base_url('assets/img/products/back/');?>'+data+'" height="50" width="50" class="text-center"/>';
                                }
                            }
                        },
                        {"data": "pcat"},{"data": "pcost"},{"data": "pmrp"},{"data": "psell"},{"data": "status"},
                        {
                            "data" : "action",
                            "orderable": false,
                            "className" : "text-center"
                        }
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>
    </body>
</html>