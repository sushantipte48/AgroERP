<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Input Read</h2>
        <table class="table">
	    <tr><td>Mid</td><td><?php echo $mid; ?></td></tr>
	    <tr><td>Mgodown</td><td><?php echo $mgodown; ?></td></tr>
	    <tr><td>Product</td><td><?php echo $product; ?></td></tr>
	    <tr><td>Stock</td><td><?php echo $stock; ?></td></tr>
	    <tr><td>Price</td><td><?php echo $price; ?></td></tr>
	    <tr><td>Ordered On</td><td><?php echo $ordered_on; ?></td></tr>
	    <tr><td>Received Date</td><td><?php echo $received_date; ?></td></tr>
	    <tr><td>Status</td><td><?php echo $status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('input') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>