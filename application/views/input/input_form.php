<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
<div class="alert alert-info display-show">
                <button class="close" data-close="alert"></button>
                <span><?php echo $message;?></span>
             </div>
    <?php }?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>AGROGODOWN <?php echo $button;?></h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Agrogodown Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Input create
                </li>
             </ul>
            <hr>
        <form action="<?php echo $action;?>" method="post" class="form-horizontal">
        <div class="form-group">
            <label class="control-label col-md-3">Manufacture_id <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
                <select name="mid" id="mid" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <?php foreach ($manu as $key) {
                        $sel = set_select('mid', $key->id, $mid['value'] == $key->id?True:False);
                        echo "<option value='".$key->id."' ".$sel.">".$key->name."</option>";
                    }?>
                </select>
               <!-- <input type="text" class="form-control" name="mid" id="mid" value="<?php echo $mid;?>" /> -->
               <span><?php echo form_error('mid')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Manufacture_godown <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
            <select name="mgodown" id="mgodown" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <?php foreach ($manu as $key) {
                        $sel = set_select('mgodown', $key->id, $mgodown['value'] == $key->id?True:False);
                        echo "<option value='".$key->id."' ".$sel.">".$key->name."</option>";
                    }?>
                </select>
               <!-- <input type="text" class="form-control" name="mgodown" id="mgodown" value="<?php echo $mgodown;?>" /> -->
               <span><?php echo form_error('mgodown')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Product <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
            <select name="product" id="product" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <?php foreach ($manu as $key) {
                        $sel = set_select('product', $key->id, $product['value'] == $key->id?True:False);
                        echo "<option value='".$key->id."' ".$sel.">".$key->name."</option>";
                    }?>
                </select>
            <!-- <input type="text" class="form-control" name="product" id="product" value="<?php echo $product;?>" /> -->
               <span><?php echo form_error('product')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Stock <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="stock" id="stock" value="<?php echo $stock;?>" />
               <span><?php echo form_error('stock')?></span>
            </div>
         </div>
         v
         <div class="form-group">
            <label class="control-label col-md-3">Price <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="price" id="price" value="<?php echo $price;?>" />
               <span><?php echo form_error('price')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Ordered_on <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="ordered_on" id="ordered_on" value="<?php echo $ordered_on;?>" />
               <span><?php echo form_error('ordered_on')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">received_date <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="received_date" id="received_date" value="<?php echo $received_date;?>" />
               <span><?php echo form_error('received_date')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">status <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <select name="status" id="status" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <option value="0">Deactive</option>
                    <option value="1">Active</option>
                </select>
               <span><?php echo form_error('status')?></span>
            </div>
         </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('input') ?>" class="btn btn-default">Cancel</a>
	</form>
    </div></div></div></div>