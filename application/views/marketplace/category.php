<!DOCTYPE html>
<html lang="en">

<head>
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,300,100' rel='stylesheet' type='text/css'>

    <!-- styles -->
    <link href="<?php echo base_url('assets/market').'/css/font-awesome.css'?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/market').'/css/bootstrap.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/market').'/css/animate.min.css'?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/market').'/css/owl.carousel.css'?>" rel="stylesheet">
    <link href="<?php echo base_url('assets/market').'/css/owl.theme.css'?>" rel="stylesheet">

    <!-- theme stylesheet -->
    <link href="<?php echo base_url('assets/market').'/css/style.default.css'?>" rel="stylesheet" id="theme-stylesheet">

    <!-- your stylesheet with modifications -->
    <link href="<?php echo base_url('assets/market').'/css/custom.css'?>" rel="stylesheet">

    <script src="<?php echo base_url('assets/market').'/js/respond.min.js'?>"></script>

    <link href="<?php echo base_url('assets/home/').'css/bootstrap.css'?>" rel="stylesheet" type="text/css" media="all">
<link href="<?php echo base_url('assets/home/').'css/style.css'?>" rel="stylesheet" type="text/css" media="all" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic' rel='stylesheet' type='text/css'>
<script src="<?php echo base_url('assets/home/').'js/jquery-1.11.1.min.js'?>"></script>
 <link rel="stylesheet" href="<?php echo base_url('assets/home/').'css/flexslider.css'?>" type="text/css" media="screen" />
<!---- start-smoth-scrolling---->
<script type="text/javascript" src="<?php echo base_url('assets/home/').'js/move-top.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/home/').'js/easing.js'?>"></script>
 <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event){     
                event.preventDefault();
                $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
            });
        });
    </script>
<!---End-smoth-scrolling---->
<link rel="stylesheet" href="<?php echo base_url('assets/home/').'css/swipebox.css'?>">
            <script src="<?php echo base_url('assets/home/').'js/jquery.swipebox.min.js'?>"></script> 
                <script type="text/javascript">
                    jQuery(function($) {
                        $(".swipebox").swipebox();
                    });
                </script>
                <!--Animation-->
<script src="<?php echo base_url('assets/home/').'js/wow.min.js'?>"></script>
<link href="<?php echo base_url('assets/home/').'css/animate.css'?>" rel='stylesheet' type='text/css' />
<script>
    new WOW().init();
</script>
<!---/End-Animation---->




</head>
<div class="header head-top" id="home">
        <div class="container">
        <div class="header-top">
        <div class="top-menu" style="width: 100%;">
        <span class="menu"><img src="<?php echo base_url('assets/home/').'images/nav.png'?>" alt=""/> </span>
        <ul style="width: 100%">
    <div style="float: left;">
    <li><a href="<?php echo base_url('home/').'index'?>" >home</a></li><label>/</label>
    <li><a href="<?php echo base_url('home/').'about'?>" >About</a></li><label>/</label>
    <li><a href="<?php echo base_url('home/').'services'?>">Services</a></li><label>/</label>
    <li><a href="<?php echo base_url('home/').'products'?>" class="active">products</a></li><label>/</label>
    <li><a href="<?php echo base_url('home/').'contact'?>">Contact</a></li>
    </div>
    <?php if($this->ion_auth->logged_in()){
        // echo '<div class="top-menu">
  //                    <li class="dropdown dropdown-user">
  //                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
  //                          <span class="username username-hide-on-mobile">'.$this->session->userdata('first_name').''.$this->session->userdata('last_name').'
  //                          </span>
  //                          <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
  //                          &nbsp;
  //                          <img alt="" class="img-circle" src="'.base_url().'assets/admin/layout4/img/avatar9.jpg"/>
  //                       </a>
  //                       <ul class="dropdown-menu dropdown-menu-default">
  //                          <li>
  //                             <a href="extra_profile.html">
  //                             <i class="icon-user"></i> My Profile </a>
  //                          </li>
  //                          <li>
  //                             <a href="page_calendar.html">
  //                             <i class="icon-calendar"></i> Change Password </a>
  //                          </li>
  //                          <li class="divider"></li>
  //                          <li>
  //                             <a href="extra_lock.html">
  //                             <i class="icon-lock"></i> Lock Screen </a>
  //                          </li>
  //                          <li>
  //                             <a href="'.base_url('index.php/auth/logout').'">
  //                             <i class="icon-key"></i> Log Out </a>
  //                          </li>
  //                       </ul>
  //                    </li>
  //                    <!-- END USER LOGIN DROPDOWN -->
  //                 </div>';
        echo '<li style="float: right;"><a href="'.base_url('auth').'">Admin Panel</a></li>
          </ul>';
    }
    else
    {
        echo '<li style="float: right;"><a href="'.base_url('auth').'">Log In</a></li>
          </ul>';
    }
    ?>
     <!-- script for menu -->
                
         <script>
         $("span.menu").click(function(){
         $(".top-menu ul").slideToggle("slow" , function(){
         });
         });
         </script>
    <!-- //script for menu -->
     </div>
        <div class="clearfix"></div>
    
    </div>
    <div class="logo logo1">
        <a href="<?php echo base_url('home/')?>">Agrovial</a>
        </div>
    </div></div>
    <div id="all" style="margin-top: 5px">

        <div id="content">
            <div class="container">
                <div class="col-md-3">
                    <!-- *** MENUS AND FILTERS ***
 _________________________________________________________ -->
                    <div class="panel panel-default sidebar-menu">

                        <div class="panel-heading " >
                            <h3 class="panel-title">Categories</h3>
                        </div>

                        <div class="panel-body">
                            <ul class="nav nav-pills nav-stacked category-menu">
                                <li>
                                    <a href="<?php echo base_url('home/').'fetch/0';?>">Fertilizers</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('home/').'fetch/1';?>">Pesticides</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('home/').'fetch/2';?>">Seeds</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url('home/').'fetch/3';?>">Farming Tools</a>
                                </li>
                            </ul>

                        </div>
                    </div>

                    <!-- *** MENUS AND FILTERS END *** -->

                    <div class="banner">
                        <a href="#">
                            <img src="<?php echo base_url('assets/market').'/img/banner.jpg'?>" alt="sales 2014" class="img-responsive">
                        </a>
                    </div>
                </div>

                <div class="col-md-9">
                    <!-- <div class="box info-bar">
                        <div class="row">
                            <div id="list" class="col-sm-12 col-md-4 products-showing">
                                Showing <strong>12</strong> of <strong>25</strong> products
                            </div>

                            <div class="col-sm-12 col-md-8  products-number-sort">
                                <div class="row">
                                    <form class="form-inline">
                                        <div class="col-md-6 col-sm-6">
                                        </div>
                                        <div class="col-md-6 col-sm-6">
                                            <div class="products-sort-by">
                                                <strong>Sort by</strong>
                                                <select name="sort-by" class="form-control">
                                                    <option>Price</option>
                                                    <option>Name</option>
                                                    <option>Sales first</option>
                                                </select>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div> -->
                    <div ></div>
                    <div id="products" class="row products">
                        <!-- /.col-md-4 -->
                        <?php if(isset($market) && $market!="")
                        {
                            echo $market;
                        }else
                        {
                            echo "<div>No Products Found. Please Come Back Later</div>";
                        } ?>
                    </div>
                    <!-- /.products -->

                    <!-- <div class="pages">

                        <p class="loadMore">
                            <a href="#" class="btn btn-primary btn-lg"><i class="fa fa-chevron-down"></i> Load more</a>
                        </p>

                        <ul class="pagination">
                            <li><a href="#">&laquo;</a>
                            </li>
                            <li class="active"><a href="#">1</a>
                            </li>
                            <li><a href="#">2</a>
                            </li>
                            <li><a href="#">3</a>
                            </li>
                            <li><a href="#">4</a>
                            </li>
                            <li><a href="#">5</a>
                            </li>
                            <li><a href="#">&raquo;</a>
                            </li>
                        </ul>
                    </div> -->


                </div>
                <!-- /.col-md-9 -->
            </div>
            <!-- /.container -->
        </div>
        <!-- /#content -->


        <!-- *** FOOTER ***
 _________________________________________________________ -->
        <div class="footer-section">
           <div class="container">
           <div class="footer-top">
         <div class="social-icons wow bounceInLeft animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
        <a href="#"><i class="icon1"></i></a>
        <a href="#"><i class="icon2"></i></a>
        <a href="#"><i class="icon3"></i></a>
        <a href="#"><i class="icon4"></i></a>
        </div>
        </div>
         <div class="footer-middle wow fadeInDown Big animated animated" data-wow-delay="0.4s">
         <div class="bottom-menu">
      <ul>
    <li><a href="<?php echo base_url('home/').'index'?>">home</a></li>
    <li><a href="<?php echo base_url('home/').'about'?>">About</a></li>
    <li><a href="<?php echo base_url('home/').'services'?>">Services</a></li>
    <li><a href="<?php echo base_url('home/').'products'?>">products</a></li>
    
    <li><a href="<?php echo base_url('home/').'contact'?>">Contact</a></li>
    </ul>
        </div>
        </div>
        <div class="footer-bottom wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
                                    <p> Copyright &copy;2015  All rights  Reserved </p>
                                    </div>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            /*
                            var defaults = {
                                containerID: 'toTop', // fading element id
                                containerHoverID: 'toTopHover', // fading element hover id
                                scrollSpeed: 1200,
                                easingType: 'linear' 
                            };
                            */
                            
                            $().UItoTop({ easingType: 'easeOutQuart' });
                            
                        });
                    </script>
                <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
                </div>
           </div>
    <!-- /#all -->
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url();?>"
        })
    })
</script>

    

    <!-- *** SCRIPTS TO INCLUDE ***
 _________________________________________________________ -->
    <script src="<?php echo base_url('assets/market').'/js/jquery-1.11.0.min.js'?>"></script>
    <script src="<?php echo base_url('assets/market').'/js/bootstrap.min.js'?>"></script>
    <script src="<?php echo base_url('assets/market').'/js/jquery.cookie.js'?>"></script>
    <script src="<?php echo base_url('assets/market').'/js/waypoints.min.js'?>"></script>
    <script src="<?php echo base_url('assets/market').'/js/modernizr.js'?>"></script>
    <script src="<?php echo base_url('assets/market').'/js/bootstrap-hover-dropdown.js'?>"></script>
    <script src="<?php echo base_url('assets/market').'/js/owl.carousel.min.js'?>"></script>
    <script src="<?php echo base_url('assets/market').'/js/front.js'?>"></script>






</body>

</html>