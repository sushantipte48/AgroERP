<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Logs Read</h2>
        <table class="table">
	    <tr><td>Uri</td><td><?php echo $uri; ?></td></tr>
	    <tr><td>Method</td><td><?php echo $method; ?></td></tr>
	    <tr><td>Params</td><td><?php echo $params; ?></td></tr>
	    <tr><td>Api Key</td><td><?php echo $api_key; ?></td></tr>
	    <tr><td>Ip Address</td><td><?php echo $ip_address; ?></td></tr>
	    <tr><td>Time</td><td><?php echo $time; ?></td></tr>
	    <tr><td>Rtime</td><td><?php echo $rtime; ?></td></tr>
	    <tr><td>Authorized</td><td><?php echo $authorized; ?></td></tr>
	    <tr><td>Response Code</td><td><?php echo $response_code; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('logs') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>