<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Logs <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Uri <?php echo form_error('uri') ?></label>
            <input type="text" class="form-control" name="uri" id="uri" placeholder="Uri" value="<?php echo $uri; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Method <?php echo form_error('method') ?></label>
            <input type="text" class="form-control" name="method" id="method" placeholder="Method" value="<?php echo $method; ?>" />
        </div>
	    <div class="form-group">
            <label for="params">Params <?php echo form_error('params') ?></label>
            <textarea class="form-control" rows="3" name="params" id="params" placeholder="Params"><?php echo $params; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Api Key <?php echo form_error('api_key') ?></label>
            <input type="text" class="form-control" name="api_key" id="api_key" placeholder="Api Key" value="<?php echo $api_key; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Ip Address <?php echo form_error('ip_address') ?></label>
            <input type="text" class="form-control" name="ip_address" id="ip_address" placeholder="Ip Address" value="<?php echo $ip_address; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Time <?php echo form_error('time') ?></label>
            <input type="text" class="form-control" name="time" id="time" placeholder="Time" value="<?php echo $time; ?>" />
        </div>
	    <div class="form-group">
            <label for="float">Rtime <?php echo form_error('rtime') ?></label>
            <input type="text" class="form-control" name="rtime" id="rtime" placeholder="Rtime" value="<?php echo $rtime; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Authorized <?php echo form_error('authorized') ?></label>
            <input type="text" class="form-control" name="authorized" id="authorized" placeholder="Authorized" value="<?php echo $authorized; ?>" />
        </div>
	    <div class="form-group">
            <label for="smallint">Response Code <?php echo form_error('response_code') ?></label>
            <input type="text" class="form-control" name="response_code" id="response_code" placeholder="Response Code" value="<?php echo $response_code; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('logs') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>