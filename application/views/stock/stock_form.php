<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
	 <div class="alert alert-info display-show">
	    <button class="close" data-close="alert"></button>
	    <span><?php echo $message;?></span>
	 </div>
	<?php }?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>STOCK <?php echo $button;?></h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Stock Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Stock Create
                </li>
             </ul>
            <hr>

        <h2 style="margin-top:0px">Output <?php echo $button?></h2>
        <form action="<?php echo $action;?>" method="post" class="form-horizontal">
	    <!-- <div class="form-group">
            <label for="int">Gid <?php echo form_error('gid')?></label>
            <input type="text" class="form-control" name="gid" id="gid" placeholder="Gid" value="<?php echo $gid;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Pid <?php echo form_error('pid')?></label>
            <input type="text" class="form-control" name="pid" id="pid" placeholder="Pid" value="<?php echo $pid;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Stock <?php echo form_error('stock')?></label>
            <input type="text" class="form-control" name="stock" id="stock" placeholder="Stock" value="<?php echo $stock;?>" />
        </div>
	    <div class="form-group">
            <label for="date">Exp Date <?php echo form_error('exp_date')?></label>
            <input type="text" class="form-control" name="exp_date" id="exp_date" placeholder="Exp Date" value="<?php echo $exp_date;?>" />
        </div> -->
        <div class="form-group">
            <label class="control-label col-md-3">G_ID<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
            <select name="gid" id="gid" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <?php foreach ($manu as $key) {
                        $sel = set_select('gid', $key->id, $gid['value'] == $key->id?True:False);
                        echo "<option value='".$key->id."' ".$sel.">".$key->name."</option>";
                    }?>
                </select>
              <!--  <input type="text" class="form-control" name="gid" id="gid" value="<?php echo $gid;?>" /> -->
               <span><?php echo form_error('gid')?></span>
            </div>
         </div>
        <div class="form-group">
            <label class="control-label col-md-3">Product ID<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <select name="pid" id="pid" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <?php foreach ($manu as $key) {
                        $sel = set_select('pid', $key->id, $pid['value'] == $key->id?True:False);
                        echo "<option value='".$key->id."' ".$sel.">".$key->name."</option>";
                    }?>
                </select>
               <!-- <input type="text" class="form-control" name="pid" id="pid" value="<?php echo $pid;?>" /> -->
               <span><?php echo form_error('pid')?></span>
            </div>
         </div>
        <div class="form-group">
            <label class="control-label col-md-3">Stock<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="stock" id="stock" value="<?php echo $stock;?>" />
               <span><?php echo form_error('stock')?></span>
            </div>
         </div>
        <div class="form-group">
            <label class="control-label col-md-3">Expire Date<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="exp_date" id="exp_date" value="<?php echo $exp_date;?>" />
               <span><?php echo form_error('exp_date')?></span>
            </div>
        </div>
	    <input type="hidden" name="id" value="<?php echo $id;?>" />
        <button type="submit" class="btn btn-primary"><?php echo $button?></button>
	    <a href="<?php echo site_url('stock')?>" class="btn btn-default">Cancel</a>
        </form></div></div></div></div></div>