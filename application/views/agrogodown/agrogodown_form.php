<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
			 <div class="alert alert-info display-show">
			    <button class="close" data-close="alert"></button>
			    <span><?php echo $message;?></span>
			 </div>
	<?php }?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>AGROGODOWN <?php echo $button;?></h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Agrogodown Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Agrogodown Create
                </li>
             </ul>
            <hr>
        <form action="<?php echo $action;?>" method="post" class="form-horizontal">
	    <!-- <div class="form-group">
            <label for="varchar">Agro Add <?php echo form_error('agro_add')?></label>
            <input type="text" class="form-control" name="agro_add" id="agro_add" placeholder="Agro Add" value="<?php echo $agro_add;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Agro Contact <?php echo form_error('agro_contact')?></label>
            <input type="text" class="form-control" name="agro_contact" id="agro_contact" placeholder="Agro Contact" value="<?php echo $agro_contact;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Capacity <?php echo form_error('capacity')?></label>
            <input type="text" class="form-control" name="capacity" id="capacity" placeholder="Capacity" value="<?php echo $capacity;?>" />
        </div> -->
        <div class="form-group">
            <label class="control-label col-md-3">Address <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="agro_add" id="agro_add" value="<?php echo $agro_add;?>" />
               <span><?php echo form_error('agro_add')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Contact <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="agro_contact" id="agro_contact" value="<?php echo $agro_contact;?>" />
               <span><?php echo form_error('agro_contact')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Capacity <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="capacity" id="capacity" value="<?php echo $capacity;?>" />
               <span><?php echo form_error('capacity')?></span>
            </div>
         </div>
	    <input type="hidden" name="id" value="<?php echo $id;?>" />
	    <button type="submit" class="btn btn-primary"><?php echo $button?></button>
	    <a href="<?php echo site_url('agrogodown')?>" class="btn btn-default">Cancel</a>
	</form>
    </div></div></div></div></div>