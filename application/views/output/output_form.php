<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
			 <div class="alert alert-info display-show">
			    <button class="close" data-close="alert"></button>
			    <span><?php echo $message;?></span>
			 </div>
	<?php }?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>OUTPUT <?php echo $button;?></h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Commodity Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Output Create
                </li>
             </ul>
            <hr>
        <h2 style="margin-top:0px">Output <?php echo $button?></h2>
        <form action="<?php echo $action;?>" method="post" class="form-horizontal" >
	    <!-- <div class="form-group">
            <label for="int">Id <?php echo form_error('id')?></label>
            <input type="text" class="form-control" name="id" id="id" placeholder="Id" value="<?php echo $id;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Shop <?php echo form_error('shop')?></label>
            <input type="text" class="form-control" name="shop" id="shop" placeholder="Shop" value="<?php echo $shop;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Oprice <?php echo form_error('oprice')?></label>
            <input type="text" class="form-control" name="oprice" id="oprice" placeholder="Oprice" value="<?php echo $oprice;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Stock <?php echo form_error('stock')?></label>
            <input type="text" class="form-control" name="stock" id="stock" placeholder="Stock" value="<?php echo $stock;?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Products <?php echo form_error('products')?></label>
            <input type="text" class="form-control" name="products" id="products" placeholder="Products" value="<?php echo $products;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Oid <?php echo form_error('oid')?></label>
            <input type="text" class="form-control" name="oid" id="oid" placeholder="Oid" value="<?php echo $oid;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Status <?php echo form_error('status')?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status;?>" />
        </div>
	    <div class="form-group">
            <label for="date">Odate <?php echo form_error('odate')?></label>
            <input type="text" class="form-control" name="odate" id="odate" placeholder="Odate" value="<?php echo $odate;?>" />
        </div> -->
         <div class="form-group">
            <label class="control-label col-md-3">ID<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="id" id="id" value="<?php echo $id;?>" />
               <span><?php echo form_error('id')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Shop<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="shop" id="shop" value="<?php echo $shop;?>" />
               <span><?php echo form_error('shop')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Output Price<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="oprice" id="oprice" value="<?php echo $oprice;?>" />
               <span><?php echo form_error('oprice')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Stock<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
              <!-- <select name="stock" id="stock" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <?php foreach ($subjects as $key) {
                        $sel = set_select('stock', $key->id, $stock['value'] == $key->id?True:False);
                        echo "<option value='".$key->id."' ".$sel.">".$key->name."</option>";
                    }?> 
                    </select> -->
               <input type="text" class="form-control" name="stock" id="stock" value="<?php echo $stock;?>" /> 
               <span><?php echo form_error('stock')?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Products<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="products" id="products" value="<?php echo $products;?>" />
               <span><?php echo form_error('products');?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Output ID<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="oid" id="oid" value="<?php echo $oid;?>" />
               <span><?php echo form_error('oid');?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Status<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="status" id="status" value="<?php echo $status;?>" />
               <span><?php echo form_error('status');?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">Output Date<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="odate" id="odate" value="<?php echo $odate;?>" />
               <span><?php echo form_error('odate');?></span>
            </div>

            </div>
	    <input type="hidden" name="id" value="<?php echo $id;?>" />
	    <button type="submit" class="btn btn-primary"><?php echo $button?></button>
	    <a href="<?php echo site_url('output')?>" class="btn btn-default">Cancel</a>
    </form></div></div></div></div></div>