<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Output Read</h2>
        <table class="table">
	    <tr><td>Id</td><td><?php echo $id; ?></td></tr>
	    <tr><td>Shop</td><td><?php echo $shop; ?></td></tr>
	    <tr><td>Oprice</td><td><?php echo $oprice; ?></td></tr>
	    <tr><td>Stock</td><td><?php echo $stock; ?></td></tr>
	    <tr><td>Products</td><td><?php echo $products; ?></td></tr>
	    <tr><td>Oid</td><td><?php echo $oid; ?></td></tr>
	    <tr><td>Status</td><td><?php echo $status; ?></td></tr>
	    <tr><td>Odate</td><td><?php echo $odate; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('output') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>