<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
 <?php if(!empty($message)){ ?>
 <div class="alert alert-info display-show">
    <button class="close" data-close="alert"></button>
    <span><?php echo $message; ?> </span>
 </div>
 <?php } ?>
 <div class="row">
	<div class="col-md-12">
	     <div class="portlet light">
				<div class="portlet-body">
				<!-- BEGIN PAGE HEAD -->
				 <div class="page-head">
				    <!-- BEGIN PAGE TITLE -->
				    <div class="page-title">
				       <h1>Manage users</h1>
				    </div>
				    <!-- END PAGE TITLE -->
				 </div>
				 <!-- END PAGE HEAD -->
				 <!-- BEGIN PAGE BREADCRUMB -->
				 <ul class="page-breadcrumb breadcrumb">
				    <li>
				       <a href="#">Home</a><i class="fa fa-circle"></i>
				    </li>
				    <li>
				       <a href="#">Admin</a><i class="fa fa-circle"></i>
				    </li>
				    <li class="active">
				       User List 
				    </li>
				 </ul>
				 <!-- END PAGE BREADCRUMB -->
				    <div class="table-toolbar">
								<div class="row">
									<div class="col-md-12">
										<div class="btn-group">
										    <a href="<?php echo base_url('auth/create_user');?>" class="btn green">New User
										    <i class="fa fa-plus"></i></a>
										</div>
									</div>
								</div>
							</div>
					<table class="table table-striped table-bordered table-hover" id="sample_1">
					<thead>
					<tr>
						<th><?php echo lang('index_fname_th');?></th>
						<th><?php echo lang('index_lname_th');?></th>
						<th><?php echo lang('index_email_th');?></th>
						<th><?php echo lang('index_groups_th');?></th>
						<th><?php echo lang('index_status_th');?></th>
						<th><?php echo lang('index_action_th');?></th>
					</tr>
					</thead>
					<tbody>
					    <?php foreach ($users as $user):?>
						<tr>
			            <td><?php echo htmlspecialchars($user->first_name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->last_name,ENT_QUOTES,'UTF-8');?></td>
			            <td><?php echo htmlspecialchars($user->email,ENT_QUOTES,'UTF-8');?></td>
						<td>
							<?php foreach ($user->groups as $group):?>
								<?php echo htmlspecialchars($group->name,ENT_QUOTES,'UTF-8') ;?><br />
			                <?php endforeach?>
						</td>
						<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, lang('index_active_link')) : anchor("auth/activate/". $user->id, lang('index_inactive_link'));?></td>
						<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
						</tr>	
						<?php endforeach;?> 
					</tbody>
					</table>
				</div>
			</div>
	</div>
 </div>			
 
 <!-- END PAGE CONTENT INNER -->
</div>