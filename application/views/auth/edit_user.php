<div class="page-content">
 <!-- BEGIN PAGE HEAD -->

 <!-- END PAGE BREADCRUMB -->
 <!-- BEGIN PAGE CONTENT INNER -->
 <?php if(!empty($message)){ ?>
 <div class="alert alert-info display-show">
    <button class="close" data-close="alert"></button>
    <span><?php echo $message; ?> </span>
 </div>
 <?php } ?>

   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
           <div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
       <h1>User Edit <small>Edit user</small></h1>
    </div>
    <!-- END PAGE TITLE -->
 </div>
 <!-- END PAGE HEAD -->
 <!-- BEGIN PAGE BREADCRUMB -->
 <ul class="page-breadcrumb breadcrumb">
    <li>
       <a href="#">Home</a><i class="fa fa-circle"></i>
    </li>
    <li>
       <a href="#">Admin</a><i class="fa fa-circle"></i>
    </li>
    <li class="active">
       User Edit 
    </li>
 </ul>
            <div class="portlet-body form">
              <!-- BEGIN FORM-->
              <form action="<?php echo base_url('auth/edit_user/'.$user->id); ?>" id="abc" class="form-horizontal" method="POST" enctype="multipart/form-data">
                <div class="form-body">
                  <div class="row">
                  <div class="col-md-9">
                 <div class="form-group">
                    <label class="control-label col-md-3">First Name <span class="required">
                    * </span>
                    </label>
                    <div class="col-md-4">
<?php echo form_input($first_name);?>
                      <span style="color: red"><?php echo form_error('first_name');?></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">Last Name <span class="required">
                    * </span>
                    </label>
                    <div class="col-md-4">
<?php echo form_input($last_name);?>
                      <span style="color: red"><?php echo form_error('last_name');?></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">Email <span class="required">
                    * </span>
                    </label>
                    <div class="col-md-4">
<?php echo form_input($email);?>
                      <span style="color: red"><?php echo form_error('email');?></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">Phone
                    </label>
                    <div class="col-md-4">
<?php echo form_input($phone);?>
                      <span style="color: red"><?php echo form_error('phone');?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Password<span class="required">
                    * </span></label>
                    <div class="col-md-4">
<?php echo form_input($password);?>
                       <span style="color: red"><?php echo form_error('password');?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Confirm Password <span class="required">
                    * </span>
                    </label>
                    <div class="col-md-4">
<?php echo form_input($password_confirm);?>
                      <span style="color: red"><?php echo form_error('password_confirm');?></span>
                    </div>
                  </div>
                <div class="form-group">
                    <label class="control-label col-md-3">Roles<span class="required">
                      * </span></label>
                    <div class="col-md-4">
                      <select name="roles" class="bs-select form-control" data-show-subtext="true">
                        <option data-icon="fa fa-lock" <?php if($roles['value']==1){echo "selected";}?> value="1">Admin</option>
                        <option data-icon="fa fa-users" <?php if($roles['value']==2){echo "selected";}?> value="2">Members</option>
                        <option data-icon="fa fa-question" <?php if($roles['value']==3){echo "selected";}?> value="3">Sub Admin</option>
                        <option data-icon="fa fa-user" <?php if($roles['value']==4){echo "selected";}?> value="4">Executive</option>
                        <option data-icon="fa fa-question" <?php if($roles['value']==6){echo "selected";}?> value="6">Manufacture</option>
                        <option data-icon="fa fa-user" <?php if($roles['value']==5){echo "selected";}?> value="5">Customer</option>
                      </select>
                    </div>
                </div> 
                
                 <div class="form-actions">
                  <div class="row">
                    <div class="col-md-offset-3 col-md-9">
                      <button type="submit" class="btn blue">Update</button>
                     <a href="<?php echo site_url('auth'); ?>" class="btn btn-default">Cancel</a>
                    </div>
                  </div>
                </div>
                </div>
                </div>
                </div>
                
              </form>
              <!-- END FORM-->
            </div>
          </div>
          <!-- END VALIDATION STATES-->
        </div>
      </div>
 
 <!-- END PAGE CONTENT INNER -->
</div>

