<div class="page-content">
 <!-- BEGIN PAGE HEAD -->
 <div class="portlet light">
            <div class="portlet-body form">
 <div class="page-head">
    <!-- BEGIN PAGE TITLE -->
    <div class="page-title">
       <h1>User Create <small>Add user</small></h1>
    </div>
    <!-- END PAGE TITLE -->
 </div>
 <!-- END PAGE HEAD -->
 <!-- BEGIN PAGE BREADCRUMB -->
 <ul class="page-breadcrumb breadcrumb">
    <li>
       <a href="#">Home</a><i class="fa fa-circle"></i>
    </li>
    <li>
       <a href="#">Admin</a><i class="fa fa-circle"></i>
    </li>
    <li class="active">
       User Create
    </li>
 </ul>
 <!-- END PAGE BREADCRUMB -->
 <!-- BEGIN PAGE CONTENT INNER -->
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
              <!-- BEGIN FORM-->
              <form action="create_user" id="abc" class="form-horizontal" method="post" enctype="multipart/form-data">
               <div class="form-body">
               <div class="row">
                <!-- <div class="col-md-3 text-center">
                    <div class="fileinput fileinput-new" data-provides="fileinput">
                        <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                         <img  name="photo" id="photo" src="<?php echo base_url()."assets/img/profiles/NoPicAvailable.png";?>" alt="Profile Photo">

                          </div>
                        <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;">
                        </div>
                        <div>
                          <span class="btn default btn-file">
                          <span class="fileinput-new">
                          Select image </span>
                          <span class="fileinput-exists">
                          Change </span>
                          <input type="file" name="file">
                          </span>
                          <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                          Remove </a>
                        </div>
                    </div>
                </div> -->
                <div class="col-md-9">
<div class="form-group">
                    <label class="control-label col-md-3">First Name <span class="required">
                    * </span>
                    </label>
                    <div class="col-md-4">
<?php echo form_input($first_name);?>
                      <span style="color: red"><?php echo form_error('first_name');?></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">Last Name <span class="required">
                    * </span>
                    </label>
                    <div class="col-md-4">
<?php echo form_input($last_name);?>
                      <span style="color: red"><?php echo form_error('last_name');?></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">Email <span class="required">
                    * </span>
                    </label>
                    <div class="col-md-4">
<?php echo form_input($email);?>
                      <span style="color: red"><?php echo form_error('email');?></span>
                    </div>
                  </div>

                  <div class="form-group">
                    <label class="control-label col-md-3">Phone
                    </label>
                    <div class="col-md-4">
<?php echo form_input($phone);?>
                      <span style="color: red"><?php echo form_error('phone');?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Password<span class="required">
                    * </span></label>
                    <div class="col-md-4">
<?php echo form_input($password);?>
                       <span style="color: red"><?php echo form_error('password');?></span>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="control-label col-md-3">Confirm Password <span class="required">
                    * </span>
                    </label>
                    <div class="col-md-4">
<?php echo form_input($password_confirm);?>
                      <span style="color: red"><?php echo form_error('password_confirm');?></span>
                    </div>
                  </div>
	<div class="form-group">
		                    <label class="control-label col-md-3">Roles<span class="required">
		                      * </span></label>
		                    <div class="col-md-4">
		                      <select name="roles" class="bs-select form-control" data-show-subtext="true">
		                        <option data-icon="fa fa-lock" value="1">Admin</option>
		                        <option data-icon="fa fa-users" value="2">Members</option>
		                        <option data-icon="fa fa-question" value="3">Sub Admin</option>
		                        <option data-icon="fa fa-user" value="4">Executive</option>
                            <option data-icon="fa fa-user" value="5">Customer</option>
                            <option data-icon="fa fa-user" value="6">Manufacrurer</option>
		                        <!-- <option data-icon="fa fa-user" value="Teacher">Teacher</option>
		                        <option data-icon="fa fa-graduation-cap" value="Student">Student</option> -->
		                      </select>
		                    </div>
		                  </div>
<div class="form-actions">
                    <div class="row">
                      <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn blue">Submit</button>
		                        <a href="<?php echo site_url('')?>" class="btn btn-default">Cancel</a>
                      </div>
                    </div>
                  </div>
                </div>


              </form>
              <!-- END FORM-->
            </div>
          </div>
          <!-- END VALIDATION STATES-->
        </div>
      </div>
  <!-- END PAGE CONTENT INNER -->
</div>