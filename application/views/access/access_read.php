<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Access Read</h2>
        <table class="table">
	    <tr><td>Key</td><td><?php echo $key; ?></td></tr>
	    <tr><td>All Access</td><td><?php echo $all_access; ?></td></tr>
	    <tr><td>Controller</td><td><?php echo $controller; ?></td></tr>
	    <tr><td>Date Created</td><td><?php echo $date_created; ?></td></tr>
	    <tr><td>Date Modified</td><td><?php echo $date_modified; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('access') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>