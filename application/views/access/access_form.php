<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">Access <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Key <?php echo form_error('key') ?></label>
            <input type="text" class="form-control" name="key" id="key" placeholder="Key" value="<?php echo $key; ?>" />
        </div>
	    <div class="form-group">
            <label for="tinyint">All Access <?php echo form_error('all_access') ?></label>
            <input type="text" class="form-control" name="all_access" id="all_access" placeholder="All Access" value="<?php echo $all_access; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Controller <?php echo form_error('controller') ?></label>
            <input type="text" class="form-control" name="controller" id="controller" placeholder="Controller" value="<?php echo $controller; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Date Created <?php echo form_error('date_created') ?></label>
            <input type="text" class="form-control" name="date_created" id="date_created" placeholder="Date Created" value="<?php echo $date_created; ?>" />
        </div>
	    <div class="form-group">
            <label for="timestamp">Date Modified <?php echo form_error('date_modified') ?></label>
            <input type="text" class="form-control" name="date_modified" id="date_modified" placeholder="Date Modified" value="<?php echo $date_modified; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('access') ?>" class="btn btn-default">Cancel</a>
	</form>
    </body>
</html>