<!DOCTYPE HTML>
<html>
<head>
<title>Agrovial Services</title>
<link href="<?php echo base_url('assets/home/').'css/bootstrap.css'?>" rel="stylesheet" type="text/css" media="all">
<link href="<?php echo base_url('assets/home/').'css/style.css'?>" rel="stylesheet" type="text/css" media="all" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Cultivation Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic' rel='stylesheet' type='text/css'>
<script src="<?php echo base_url('assets/home/').'js/jquery-1.11.1.min.js'?>"></script>
 <link rel="stylesheet" href="<?php echo base_url('assets/home/').'css/flexslider.css'?>" type="text/css" media="screen" />
<!---- start-smoth-scrolling---->
<script type="text/javascript" src="<?php echo base_url('assets/home/').'js/move-top.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/home/').'js/easing.js'?>"></script>
 <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
<!---End-smoth-scrolling---->
<link rel="stylesheet" href="<?php echo base_url('assets/home/').'css/swipebox.css'?>">
			<script src="<?php echo base_url('assets/home/').'js/jquery.swipebox.min.js'?>"></script> 
			    <script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
				</script>
				<!--Animation-->
<script src="<?php echo base_url('assets/home/').'js/wow.min.js'?>"></script>
<link href="<?php echo base_url('assets/home/').'css/animate.css'?>" rel='stylesheet' type='text/css' />
<script>
	new WOW().init();
</script>
<!---/End-Animation---->


</head>
<body>
  <div class="header head-top " id="home">
		<div class="container">
		<div class="header-top">
		<div class="top-menu" style="width: 100%;">
		<span class="menu"><img src="<?php echo base_url('assets/home/').'images/nav.png'?>" alt=""/> </span>
		<ul style="width: 100%">
	<div style="float: left;">
   	<li><a href="<?php echo base_url('home/').'index'?>">home</a></li><label>/</label>
    <li><a href="<?php echo base_url('home/').'about'?>" >About</a></li><label>/</label>
  	<li><a href="<?php echo base_url('home/').'services'?>" class="active">Services</a></li><label>/</label>
    <li><a href="<?php echo base_url('home/').'products'?>">products</a></li><label>/</label>
   	<li><a href="<?php echo base_url('home/').'contact'?>">Contact</a></li>
    </div>
	<?php if($this->ion_auth->logged_in()){
		// echo '<div class="top-menu">
  //                    <li class="dropdown dropdown-user">
  //                       <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
  //                          <span class="username username-hide-on-mobile">'.$this->session->userdata('first_name').''.$this->session->userdata('last_name').'
  //                          </span>
  //                          <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
  //                          &nbsp;
  //                          <img alt="" class="img-circle" src="'.base_url().'assets/admin/layout4/img/avatar9.jpg"/>
  //                       </a>
  //                       <ul class="dropdown-menu dropdown-menu-default">
  //                          <li>
  //                             <a href="extra_profile.html">
  //                             <i class="icon-user"></i> My Profile </a>
  //                          </li>
  //                          <li>
  //                             <a href="page_calendar.html">
  //                             <i class="icon-calendar"></i> Change Password </a>
  //                          </li>
  //                          <li class="divider"></li>
  //                          <li>
  //                             <a href="extra_lock.html">
  //                             <i class="icon-lock"></i> Lock Screen </a>
  //                          </li>
  //                          <li>
  //                             <a href="'.base_url('index.php/auth/logout').'">
  //                             <i class="icon-key"></i> Log Out </a>
  //                          </li>
  //                       </ul>
  //                    </li>
  //                    <!-- END USER LOGIN DROPDOWN -->
  //                 </div>';
		echo '<li style="float: right;"><a href="'.base_url('auth').'">Admin Panel</a></li>
          </ul>';
	}
	else
	{
		echo '<li style="float: right;"><a href="'.base_url('auth').'">Log In</a></li>
          </ul>';
	}
	?>
     <!-- script for menu -->
				
		 <script>
		 $("span.menu").click(function(){
		 $(".top-menu ul").slideToggle("slow" , function(){
		 });
		 });
		 </script>
	<!-- //script for menu -->
     </div>
		<div class="clearfix"></div>
	
	</div>
	<div class="logo logo1">
		<a href="<?php echo base_url('home/')?>">Agrovial</a>
		</div>
		
</div>
</div>
		 <div class="content">
		 		 <div class="services-section">
		 	<div class="container">
						<h3> our services</h3>
						<div class="services-grids">
						<div class="service1">
						<div class="col-md-4 services-grid wow bounceInLeft animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
						 <a href="<?php echo base_url('assets/home/').'images/p7.jpg'?>" class="swipebox"><img src="<?php echo base_url('assets/home/').'images/p7.jpg'?>" class="img-responsive" alt="" /></a>
						 <h5>Delivery Of products</h5>
					<p></p>
						<a href="#" class="button1 hvr-shutter-in-vertical">read more</a>
						</div>
						<div class="col-md-4 services-grid">
						<a href="<?php echo base_url('assets/home/').'images/p8.jpg'?>" class="swipebox"><img src="<?php echo base_url('assets/home/').'images/p8.jpg'?>" class="img-responsive" alt="" /></a>
						<h5>High Quality products</h5>
					<p></p>
						<a href="#" class="button1 hvr-shutter-in-vertical">read more</a>
						</div>
						<div class="col-md-4 services-grid wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
						<a href="<?php echo base_url('assets/home/').'images/p9.jpg'?>" class="swipebox"><img src="<?php echo base_url('assets/home/').'images/p9.jpg'?>" class="img-responsive" alt="" /></a>
						<h5>Best in clas service</h5>
					<p></p>
						<a href="#" class="button1 hvr-shutter-in-vertical">read more</a>
						</div>
						</div>
			<div class="clearfix"></div>
						</div>
						</div>
						</div>
					  </div>
		   <!--  -->	
		   </div>
		   <!--  -->
		   <div class="footer-section">
		   <div class="container">
		   <div class="footer-top">
		 <!-- <div class="social-icons wow bounceInLeft animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
		<a href="#"><i class="icon1"></i></a>
		<a href="#"><i class="icon2"></i></a>
		<a href="#"><i class="icon3"></i></a>
		<a href="#"><i class="icon4"></i></a>
		</div> -->
		</div>
		 <div class="footer-middle wow fadeInDown Big animated animated" data-wow-delay="0.4s">
		 <div class="bottom-menu">
      <ul>
   	<li><a href="<?php echo base_url('home/').'index'?>">home</a></li>
    <li><a href="<?php echo base_url('home/').'about'?>">About</a></li>
  	<li><a href="<?php echo base_url('home/').'services'?>">Services</a></li>
	<li><a href="<?php echo base_url('home/').'products'?>">products</a></li>
   	<li><a href="<?php echo base_url('home/').'contact'?>">Contact</a></li>
    </ul>
		</div>
		</div>
		<div class="footer-bottom wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
									<p> Copyright &copy;2017  All rights  Reserved </p>
									</div>
					<script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
				</div>
		   </div>

 </body>
</html> 