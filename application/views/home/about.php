<!--
Au<!--
Author: W3layouts
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Agrovial AboutUs</title>
<link href="<?php echo base_url('assets/home/').'css/bootstrap.css'?>" rel="stylesheet" type="text/css" media="all">
<link href="<?php echo base_url('assets/home/').'css/style.css'?>" rel="stylesheet" type="text/css" media="all" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<link href='http://fonts.googleapis.com/css?family=Lobster' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Josefin+Sans:100,300,400,600,700,100italic,300italic,400italic,600italic,700italic' rel='stylesheet' type='text/css'>
<script src="<?php echo base_url('assets/home/').'js/jquery-1.11.1.min.js'?>"></script>
 <link rel="stylesheet" href="<?php echo base_url('assets/home/').'css/flexslider.css'?>" type="text/css" media="screen" />
<!---- start-smoth-scrolling---->
<script type="text/javascript" src="<?php echo base_url('assets/home/').'js/move-top.js'?>"></script>
<script type="text/javascript" src="<?php echo base_url('assets/home/').'js/easing.js'?>"></script>
 <script type="text/javascript">
		jQuery(document).ready(function($) {
			$(".scroll").click(function(event){		
				event.preventDefault();
				$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
			});
		});
	</script>
<!---End-smoth-scrolling---->
<link rel="stylesheet" href="<?php echo base_url('assets/home/').'css/swipebox.css'?>">
			<script src="<?php echo base_url('assets/home/').'js/jquery.swipebox.min.js'?>"></script> 
			    <script type="text/javascript">
					jQuery(function($) {
						$(".swipebox").swipebox();
					});
				</script>
				<!--Animation-->
<script src="<?php echo base_url('assets/home/').'js/wow.min.js'?>"></script>
<link href="<?php echo base_url('assets/home/').'css/animate.css'?>" rel='stylesheet' type='text/css' />
<script>
	new WOW().init();
</script>
<!---/End-Animation---->


</head>
<body>
  <div class="header head-top" id="home">
		<div class="container">
		<div class="header-top">
		<div class="top-menu" style="width: 100%">
  <span class="menu"><img src="<?php echo base_url('assets/home/').'images/nav.png'?>" alt=""/> </span>
<ul style="width: 100%">
	<div style="float: left;">
   	<li><a href="<?php echo base_url('home/').'index'?>">home</a></li><label>/</label>
    <li><a href="<?php echo base_url('home/').'about'?>" class="active">About</a></li><label>/</label>
  	<li><a href="<?php echo base_url('home/').'services'?>">Services</a></li><label>/</label>
    <li><a href="<?php echo base_url('home/').'products'?>">products</a></li><label>/</label>
   	<li><a href="<?php echo base_url('home/').'contact'?>">Contact</a></li>
    </div>
	<?php if($this->ion_auth->logged_in()){
		echo '<li style="float: right;"><a href="'.base_url('auth').'">Admin Panel</a></li>
          </ul>';
	}
	else
	{
		echo '<li style="float: right;"><a href="'.base_url('auth').'">Log In</a></li>
          </ul>';
	}
	?>
	</div>
	 <!-- script for menu -->
				
		 <script>
		 $("span.menu").click(function(){
		 $(".top-menu ul").slideToggle("slow" , function(){
		 });
		 });
		 </script>
	<!-- //script for menu -->
		<div class="clearfix"></div>
	
	</div>
	<div class="logo logo1">
		<a href="<?php echo base_url('home/')?>">Agrovial</a>
		</div>
		
</div>
</div>
		 <div class="content">
		 <div class="aboutus-section">
		 <div class="container">
		 <h3>about</h3>
		 <div class="aboutus-grids">
		 <div class="col-md-5 left-grid wow bounceInLeft animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
		 <div class="aboutus1">
		 <h4>Objectives</h4>
		 <ul>
	<li>Provide high quality product</li>
	<li>Providing satisfactory services to our customers</li>
	<li>To render assistance to individual in field of agriculture</li>
	<li>To identify and promote post harvest unit</li>
	<li>Promoting and updating our customers with benefical products</li>
	</ul>
		 </div>
		 <!-- <div class="aboutus2">
		 <h4>testimonials</h4>
		 <div class="ab1">
		 <p>suadaorcnec sit amet eroorem ipsum dol. Or sit amt consc tetuer aiing elit. Mauris fermentum tumagna. Sed laoreet aliquam leo. Ut te dolor dapibus eget elentu vel cursus eleifend elit. Aenean auctor wrna. Aliqua volutpat. Duis ac turpis. Integer rutrum ante eu lacuest um liberoisl porta vel sceleisque eget malesuada at neque. Vivam nibhus  metus</p>
		 <a href="#">JOHN ANDERSON</a>
		 </div>
		  <div class="ab2">
		 <p> sit amet eroorem ipsum dol. Or sit amt consc tetuer aiing elit. Mauris fermentum tumagna. Sed laoreet aliquam leo. Ut te dolor dapibus eget elentu vel cursus eleifend elit. Aenean auctor wrna. Aliqua volutpat. Duis ac turpis. Integer rutrum ante eu lacuest um liberoisl porta vel sceleisque eget malesuada at neque. Vivam nibhus  metus</p>
		  <a href="#">OLIVIA GROSH</a>
		</div>
		</div> -->
		 <div class="clearfix"></div>
		 </div>
 <div class="col-md-7 right-grid wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
      <div class="aboutus3">
       <h5></h5>
       <p>The shortcut pleasents take inoder to yield crops at great quanlity and higt quality by using pesticides is not much profitable in the current senario so our main aim is to provide organic seeds which would definitely not degorate the nutrition value and ph acidity of the soil in the fields which would profitable to pleasents as well as to the consumers comsuming them and we decided to introduce our protal by observing the increase the demand in market for organics and pesticides free crops </p>
	</div>
	  <div class="aboutus4">
       <h4>our team</h4>
      <div class="col-md-3 team-grid">
	<img src="<?php echo base_url('assets/home/').'images/p19.jpg'?>" class="img-responsive" alt="" />
	  <h5>Sushant Ipte</h5>
      </div>
	   <div class="col-md-3 team-grid">
	   <img src="<?php echo base_url('assets/home/').'images/p20.jpg'?>" class="img-responsive" alt="" />
	  <h5>Murtuza Barodawala</h5>
      </div>
	   <div class="col-md-3 team-grid">
	  <img src="<?php echo base_url('assets/home/').'images/p21.jpg'?>" class="img-responsive" alt="" />
	  <h5>Riddhi Agarwal</h5>
      </div>
      <div class="col-md-3 team-grid">
	  <img src="<?php echo base_url('assets/home/').'images/p21.jpg'?>" class="img-responsive" alt="" />
	  <h5>Shrishti Kanchan</h5>
      </div>
      </div>	  
		 <div class="clearfix"></div>
		 </div>
			</div>
			</div>
			</div>
		  <div class="categories-section">
		   <div class="container">
		 <div class="col-md-6 cat wow bounceIn animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
		   <h3>Categories</h3>
		   <ul>
	<li>Food Crops (Wheat, Maize, Rice, Millets and Pulses )</li>
	<li>Cash Crops (Sugarcane, Tobacco, Cotton, Jute and Oilseeds)</li>
	<li>Plantation Crops (Coffee, Coconut, Tea, and Rubber.)</li>
	<li>Horticulture crops (Fruits and Vegetables)</li>
	<li>Agricultural tools</li>
	</ul>
	</div>
		 <div class="col-md-6 cont wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
		 <h3>contact</h3>
		 <ul>
		<li><i class="phone"></i></li>
		<li><p>+91-9819108192</p>
		<p>+91-9824627836</p></li>
		</ul>
		<ul>
	   <li><i class="smartphone"></i></li>
		<li><p>Parel, Mumbai</p>
		<p> Maharashtra, India</p></li>
		</ul>
		<ul>
		<li><i class="message"></i></li>
		<li><a href="mailto:example@mail.com">agrovivail@gmail.com</a>
         <a href="mailto:example@mail.com">agrohelpdesk@gmail.com</a></li>
		</ul>
		</div>
		 <div class="clearfix"></div>
		  </div>
		   </div>
		   </div>
		   <div class="footer-section">
		   <div class="container">
		   <div class="footer-top">
		 <!-- <div class="social-icons wow bounceInLeft animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
		<a href="#"><i class="icon1"></i></a>
		<a href="#"><i class="icon2"></i></a>
		<a href="#"><i class="icon3"></i></a>
		<a href="#"><i class="icon4"></i></a>
		</div> -->
		</div>
		 <div class="footer-middle wow fadeInDown Big animated animated" data-wow-delay="0.4s">
		 <div class="bottom-menu">
      <ul>
   	<li><a href="<?php echo base_url('home/').'index'?>">home</a></li>
    <li><a href="<?php echo base_url('home/').'about'?>">About</a></li>
  	<li><a href="<?php echo base_url('home/').'services'?>">Services</a></li>
	<li><a href="<?php echo base_url('home/').'products'?>">products</a></li>
   	<li><a href="<?php echo base_url('home/').'contact'?>">Contact</a></li>
    </ul>
		</div>
		</div>
		<div class="footer-bottom wow bounceInRight animated" data-wow-delay="0.4s" style="visibility: visible; -webkit-animation-delay: 0.4s;">
									<p> Copyright &copy;2017  All rights  Reserved </p>
									</div>
					<script type="text/javascript">
						$(document).ready(function() {
							/*
							var defaults = {
					  			containerID: 'toTop', // fading element id
								containerHoverID: 'toTopHover', // fading element hover id
								scrollSpeed: 1200,
								easingType: 'linear' 
					 		};
							*/
							
							$().UItoTop({ easingType: 'easeOutQuart' });
							
						});
					</script>
				<a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
				</div>
		   </div>

 </body>
</html> 