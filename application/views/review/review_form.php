<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
<?php if (!empty($message)) {?>
	 <div class="alert alert-info display-show">
	    <button class="close" data-close="alert"></button>
	    <span><?php echo $message;?></span>
	 </div>
	<?php }?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>REVIEW <?php echo $button;?></h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Review Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Review Create
                </li>
             </ul>
            <hr>
        <h2 style="margin-top:0px">Output <?php echo $button?></h2>
        <form action="<?php echo $action;?>" method="post" class="form-horizontal">
	    <!-- <div class="form-group">
            <label for="int">Pid <?php echo form_error('pid')?></label>
            <input type="text" class="form-control" name="pid" id="pid" placeholder="Pid" value="<?php echo $pid;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Rating <?php echo form_error('rating')?></label>
            <input type="text" class="form-control" name="rating" id="rating" placeholder="Rating" value="<?php echo $rating;?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Desc <?php echo form_error('desc')?></label>
            <input type="text" class="form-control" name="desc" id="desc" placeholder="Desc" value="<?php echo $desc;?>" />
        </div>
	    <div class="form-group">
            <label for="int">Status <?php echo form_error('status')?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status;?>" />
        </div> -->
        <div class="form-group">
            <label class="control-label col-md-3">Product ID <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
            <select name="pid" id="pid" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <?php foreach ($manu as $key) {
                        $sel = set_select('pid', $key->id, $pid['value'] == $key->id?True:False);
                        echo "<option value='".$key->id."' ".$sel.">".$key->name."</option>";
                    }?>
                </select>
               <!-- <input type="text" class="form-control" name="pid" id="pid" value="<?php echo $pid;?>" /> -->
               <span><?php echo form_error('pid')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Rating <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="rating" id="rating" value="<?php echo $rating;?>" />
               <span><?php echo form_error('rating')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Description <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="desc" id="desc" value="<?php echo $desc;?>" />
               <span><?php echo form_error('desc')?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Status <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="status" id="status" value="<?php echo $status;?>" />
               <span><?php echo form_error('status')?></span>
            </div>
        </div>
        
        <input type="hidden" name="id" value="<?php echo $id;?>" />
	    <button type="submit" class="btn btn-primary"><?php echo $button?></button>
	    <a href="<?php echo site_url('review')?>" class="btn btn-default">Cancel</a>
        </form></div></div></div></div></div>