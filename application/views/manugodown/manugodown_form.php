<div class="alert alert-info display-show">
    <button class="close" data-close="alert"></button>
    <span><?php echo $message; ?> </span>
 </div>
 <?php } ?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>Manufacture Godown</h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->   
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Master</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Manugodown Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Manugodown Create 
                </li>
             </ul>
            <hr>
        <h2 style="margin-top:0px">Manugodown <?php echo $button ?></h2>
        <form action="<?php echo $action; ?>" method="post">
	    <!--<div class="form-group">
            <label for="int">Mid <?php echo form_error('mid') ?></label>
            <input type="text" class="form-control" name="mid" id="mid" placeholder="Mid" value="<?php echo $mid; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Gadd <?php echo form_error('gadd') ?></label>
            <input type="text" class="form-control" name="gadd" id="gadd" placeholder="Gadd" value="<?php echo $gadd; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Contact <?php echo form_error('contact') ?></label>
            <input type="text" class="form-control" name="contact" id="contact" placeholder="Contact" value="<?php echo $contact; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Status <?php echo form_error('status') ?></label>
            <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?php echo $status; ?>" />
        </div>-->
         <div class="form-group">
            <label class="control-label col-md-3">MANUFACTURER<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
             <select name="mid" id="mid" type="text" class="form-control" value="">
                    <option value="">Select</option>
                    <?php foreach ($manu as $key) {
                        $sel = set_select('mid', $key->id, $mid['value'] == $key->id?True:False);
                        echo "<option value='".$key->id."' ".$sel.">".$key->name."</option>";
                    }?>
                </select>
               <!-- <input type="text" class="form-control" name="mid" id="Mid" value="<?php echo $mid; ?>" /> -->
               <span><?php echo form_error('mid') ?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">GODOWN ADD<span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="gadd" id="gadd" value="<?php echo $gadd; ?>" />
               <span><?php echo form_error('gadd') ?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">CONTACT <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="contact" id="contact" value="<?php echo $contact; ?>" />
               <span><?php echo form_error('contact') ?></span>
            </div>
         </div>
          <div class="form-group">
            <label class="control-label col-md-3">STATUS <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
               <input type="text" class="form-control" name="status" id="status" value="<?php echo $status; ?>" />
               <span><?php echo form_error('status') ?></span>
            </div>
         </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('manugodown') ?>" class="btn btn-default">Cancel</a>
	</form>
    </div></div></div></div>