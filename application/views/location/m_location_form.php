<div class="page-content">
 <!-- BEGIN PAGE CONTENT INNER -->
 <?php if(!empty($message)){ ?>
 <div class="alert alert-info display-show">
    <button class="close" data-close="alert"></button>
    <span><?php echo $message; ?> </span>
 </div>
 <?php } ?>
   <div class="row">
        <div class="col-md-12">
          <!-- BEGIN VALIDATION STATES-->
          <div class="portlet light">
            <div class="portlet-body form">
             <!-- BEGIN PAGE HEAD -->
             <div class="page-head">
                <!-- BEGIN PAGE TITLE -->
                <div class="page-title">
                   <h1>LOCATION <?php echo $title; ?></h1>
                </div>
                <!-- END PAGE TITLE -->
             </div>
             <!-- END PAGE HEAD -->   
             <!-- BEGIN PAGE BREADCRUMB -->
             <ul class="page-breadcrumb breadcrumb">
                <li>
                   <a href="#">Home</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Masters</a><i class="fa fa-circle"></i>
                </li>
                <li>
                   <a href="#">Location Master</a><i class="fa fa-circle"></i>
                </li>
                <li class="active">
                   Location Create 
                </li>
             </ul>
            <hr>
             <!-- END PAGE BREADCRUMB -->
        <form action="<?php echo $action; ?>" method="post" class="form-horizontal"> 
         <div class="form-group">
              <label for="inputEmail3" class="col-sm-3 control-label">District <span class="required">
            * </span></label>
              <div class="col-sm-4">
                  <input type="hidden" id="District" data-val="true" name="District" class="form-control select2" value="<?php echo $Id; ?>" >
                  <span><?php echo form_error('District') ?></span>
              </div>
         </div> 
         <div class="form-group">
            <label class="control-label col-md-3">Market Name <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
              <input type="text" class="form-control" name="MarketName1" id="MarketName1" value="<?php echo $MarketName1; ?>" />
              <span><?php echo form_error('MarketName1') ?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Pin <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
              <input type="text" class="form-control" name="Pin" id="Pin" value="<?php echo $Pin; ?>" />
              <span><?php echo form_error('Pin') ?></span>
            </div>
         </div>
         <div class="form-group">
            <label class="control-label col-md-3">Status <span class="required">
            * </span>
            </label>
            <div class="col-md-4">
              <select class="form-control" name="Status" id="Status">
                <option value="">Select One</option>
                <option value="1" <?php echo $Status=="1"?"Selected":""; ?>>Active</option>
                <option value="2" <?php echo $Status=="2"?"Selected":""; ?>>Inactive</option>
              </select>
               <span><?php echo form_error('Status') ?></span>
            </div>
         </div>
         <div class="form-actions">
          <div class="row">
            <div class="col-md-offset-3 col-md-9">
            <input type="hidden" name="Id" value="<?php echo $Id; ?>" /> 
            <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
            <a href="<?php echo site_url('location') ?>" class="btn btn-default">Cancel</a>
            </div>
          </div>
        </div>    
	</form>
</div></div></div></div></div>