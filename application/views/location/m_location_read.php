<!doctype html>
<html>
    <head>
        <title>harviacode.com - codeigniter crud generator</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            body{
                padding: 15px;
            }
        </style>
    </head>
    <body>
        <h2 style="margin-top:0px">M_location Read</h2>
        <table class="table">
	    <tr><td>Code</td><td><?php echo $Code; ?></td></tr>
	    <tr><td>MarketName</td><td><?php echo $MarketName; ?></td></tr>
	    <tr><td>Country</td><td><?php echo $Country; ?></td></tr>
	    <tr><td>State</td><td><?php echo $State; ?></td></tr>
	    <tr><td>District</td><td><?php echo $District; ?></td></tr>
	    <tr><td>CreatedOn</td><td><?php echo $CreatedOn; ?></td></tr>
	    <tr><td>CreatedBy</td><td><?php echo $CreatedBy; ?></td></tr>
	    <tr><td>ModifyOn</td><td><?php echo $ModifyOn; ?></td></tr>
	    <tr><td>ModifyBy</td><td><?php echo $ModifyBy; ?></td></tr>
	    <tr><td>Status</td><td><?php echo $Status; ?></td></tr>
	    <tr><td></td><td><a href="<?php echo site_url('location') ?>" class="btn btn-default">Cancel</a></td></tr>
	</table>
        </body>
</html>