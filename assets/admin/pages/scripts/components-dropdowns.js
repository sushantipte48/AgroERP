var ComponentsDropdowns = function () {
    var handleSelect2 = function () {

        $('#select2_sample1').select2({
            placeholder: "Select an option",
            allowClear: true
        });

        $('#subject').select2({
            placeholder: "Select a Subject",
            allowClear: true
        });
        $('#location').select2({
            placeholder: "Select a Location",
            allowClear: true
        });

        $("#select2_sample3").select2({
            placeholder: "Select...",
            allowClear: true,
            minimumInputLength: 1,
            query: function (query) {
                var data = {
                    results: []
                }, i, j, s;
                for (i = 1; i < 5; i++) {
                    s = "";
                    for (j = 0; j < i; j++) {
                        s = s + query.term;
                    }
                    data.results.push({
                        id: query.term + i,
                        text: s
                    });
                }
                query.callback(data);
            }
        });

        function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='" + Metronic.getGlobalImgPath() + "flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }
        $("#select2_sample4").select2({
            placeholder: "Select a Country",
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });

        $("#select2_sample5").select2({
            tags: ["red", "green", "blue", "yellow", "pink"]
        });


        function movieFormatResult(movie) {
            var markup = "<table class='movie-result'><tr>";
            if (movie.posters !== undefined && movie.posters.thumbnail !== undefined) {
                markup += "<td valign='top'><img src='" + movie.posters.thumbnail + "'/></td>";
            }
            markup += "<td valign='top'><h5>" + movie.title + "</h5>";
            if (movie.critics_consensus !== undefined) {
                markup += "<div class='movie-synopsis'>" + movie.critics_consensus + "</div>";
            } else if (movie.synopsis !== undefined) {
                markup += "<div class='movie-synopsis'>" + movie.synopsis + "</div>";
            }
            markup += "</td></tr></table>"
            return markup;
        }

        function movieFormatSelection(movie) {
            return movie.title;
        }

        $("#select2_sample6").select2({

            placeholder: "Search for a movie",
            minimumInputLength: 1,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "http://api.rottentomatoes.com/api/public/v1.0/movies.json",
                dataType: 'jsonp',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 10,
                        apikey: "ju6z9mjyajq2djue3gbvv26t" // please do not use so this example keeps working
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {
                        results: data.movies
                    };
                }
            },
            initSelection: function (element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected movie's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the movie name is shown preselected
                var id = $(element).val();
                if (id !== "") {
                    $.ajax("http://api.rottentomatoes.com/api/public/v1.0/movies/" + id + ".json", {
                        data: {
                            apikey: "ju6z9mjyajq2djue3gbvv26t"
                        },
                        dataType: "jsonp"
                    }).done(function (data) {
                        callback(data);
                    });
                }
            },
            formatResult: movieFormatResult, // omitted for brevity, see the source of this page
            formatSelection: movieFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            
            escapeMarkup: function (m) {
                return m;
            } // we do not want to escape markup since we are displaying html in results
        });
    }

    var handleSelect2Modal = function () {

        $('#select2_sample_modal_1').select2({
            placeholder: "Select an option",
            allowClear: true
        });

        $('#select2_sample_modal_2').select2({
            placeholder: "Select a State",
            allowClear: true
        });

        $("#select2_sample_modal_3").select2({
            allowClear: true,
            minimumInputLength: 1,
            query: function (query) {
                var data = {
                    results: []
                }, i, j, s;
                for (i = 1; i < 5; i++) {
                    s = "";
                    for (j = 0; j < i; j++) {
                        s = s + query.term;
                    }
                    data.results.push({
                        id: query.term + i,
                        text: s
                    });
                }
                query.callback(data);
            }
        });

        function format(state) {
            if (!state.id) return state.text; // optgroup
            return "<img class='flag' src='" + Metronic.getGlobalImgPath() + "flags/" + state.id.toLowerCase() + ".png'/>&nbsp;&nbsp;" + state.text;
        }
        $("#select2_sample_modal_4").select2({
            allowClear: true,
            formatResult: format,
            formatSelection: format,
            escapeMarkup: function (m) {
                return m;
            }
        });

        $("#select2_sample_modal_5").select2({
            tags: ["red", "green", "blue", "yellow", "pink"]
        });


        function districtFormatResult(district) {
            var markup = "<table class='movie-result'><tr>";
                markup += "<td valign='top'>" + district.MarketName + " <small> ( "+ district.State +" )</small>";
                markup += "</td></tr></table>"
            return markup;
        }

        function districtFormatSelection(district) {
            return district.MarketName;
        }

        $("#District").select2({
            placeholder: "Search for a district",
            minimumInputLength: 1,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "http://localhost/git/AgroERP/location/get_like_district/",
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 10,
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {
                        results: data.result
                    };
                }
            },
            initSelection: function(element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected repository's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the repository name is shown preselected
                var id = $(element).val();
                if (id !== "") {
                    $.ajax("http://localhost/git/AgroERP/location/get_district_by_id/" + id,
                    {
                        dataType: "json"
                    }).done(function(data) { 
                        callback(data);
                    });
                }
            },
            formatResult: districtFormatResult, // omitted for brevity, see the source of this page
            formatSelection: districtFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            id: function(item) {
            return item['Id'];
            },
            escapeMarkup: function (m) {
             return m;
            } // we do not want to escape markup since we are displaying html in results
        });

        function marketNameFormatResult(market) {
            var markup = "<table class='movie-result'><tr>";
                markup += "<td valign='top'><strong>" + market.MarketName + " </strong></td></tr>";
                markup += "<tr><td valign='top'><small> &#8226;  "+ market.District +" &#8226; "+ market.State +" &#8226; "+ market.Country +" </small></td></tr>";
                markup += "</table>"
            return markup;
        }

        function marketNameFormatSelection(market) {
            return market.MarketName;
        }

        $("#MarketName").select2({
            placeholder: "Search for a market",
            minimumInputLength: 1,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "http://localhost/git/AgroERP/pricepoint/get_like_Market/",
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 10,
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {
                        results: data.result
                    };
                }
            },
            initSelection: function(element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected repository's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the repository name is shown preselected
                var id = $(element).val();
                if (id !== "") {
                    $.ajax("http://localhost/git/AgroERP/pricepoint/get_market_by_id/" + id,
                    {
                        dataType: "json"
                    }).done(function(data) { 
                        callback(data);
                    });
                }
            },
            formatResult: marketNameFormatResult, // omitted for brevity, see the source of this page
            formatSelection: marketNameFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            id: function(item) {
                return item['Id'];
            },
            escapeMarkup: function (m) {
             return m;
            } // we do not want to escape markup since we are displaying html in results
        });

        //Price Point 
        
        function pricepointFormatResult(market) {
            var markup = "<table class='movie-result'><tr>";
                markup += "<td valign='top'><strong>" + market.MarketName + " </strong></td></tr>";
                markup += "<tr><td valign='top'><small> &#8226;  "+ market.District +" &#8226; "+ market.State +" &#8226; "+ market.Country +" </small></td></tr>";
                markup += "</table>"
            return markup;
        }

        function pricepointFormatSelection(market) {
            return market.MarketName;
        }

        $("#PricePoint").select2({
            placeholder: "Search for a price point",
            minimumInputLength: 1,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "http://localhost/git/AgroERP/pricepoint/get_like_Market/",
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 10,
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {
                        results: data.result
                    };
                }
            },
            initSelection: function(element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected repository's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the repository name is shown preselected
                var id = $(element).val();
                if (id !== "") {
                    $.ajax("http://localhost/git/AgroERP/pricepoint/get_market_by_id/" + id,
                    {
                        dataType: "json"
                    }).done(function(data) { 
                        callback(data);
                    });
                }
            },
            formatResult: pricepointFormatResult, // omitted for brevity, see the source of this page
            formatSelection: pricepointFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            id: function(item) {
                return item['Id'];
            },
            escapeMarkup: function (m) {
             return m;
            } // we do not want to escape markup since we are displaying html in results
        });

        function commodityFormatResult(commodity) {
            var markup = "<table class='movie-result'><tr>";
                markup += "<td valign='top'><strong>" + commodity.Name + " </strong></td></tr>";
                markup += "<tr><td valign='top'><small> &#8226;  "+ commodity.ComplexName +" &#8226; "+ commodity.Variety +" &#8226; "+ commodity.Grade +" </small></td></tr>";
                markup += "</table>"
            return markup;
        }

        function commodityFormatSelection(commodity) {
            return commodity.Name;
        }

        $("#Commodity").select2({
            placeholder: "Search for a commodity",
            minimumInputLength: 1,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "http://localhost/git/AgroERP/pricepoint/get_like_commodity/",
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 50,
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {
                        results: data.result
                    };
                }
            },
            initSelection: function(element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected repository's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the repository name is shown preselected
                var id = $(element).val();
                if (id !== "") {
                    $.ajax("http://localhost/git/AgroERP/pricepoint/get_commodity_by_id/" + id,
                    {
                        dataType: "json"
                    }).done(function(data) { 
                        callback(data);
                    });
                }
            },
            formatResult: commodityFormatResult, // omitted for brevity, see the source of this page
            formatSelection: commodityFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            id: function(item) {
                return item['Id'];
            },
            escapeMarkup: function (m) {
             return m;
            } // we do not want to escape markup since we are displaying html in results
        });

        function pricepointFormatResult(pricepoint) {
            var markup = "<table class='movie-result'><tr>";
                markup += "<td valign='top'><strong>" + pricepoint.Code + " </strong></td></tr>";
                //markup += "<tr><td valign='top'><small> &#8226;  "+ commodity.ComplexName +" &#8226; "+ commodity.Variety +" &#8226; "+ commodity.Grade +" </small></td></tr>";
                markup += "</table>"
            return markup;
        }

        function pricepointFormatSelection(pricepoint) {
            return pricepoint.Name;
        }

        $("#Pricepoint").select2({
            placeholder: "Search for a pricepoint",
            minimumInputLength: 1,
            ajax: { // instead of writing the function to execute the request we use Select2's convenient helper
                url: "http://localhost/git/AgroERP/contract/get_like_pricepoint/",
                dataType: 'json',
                data: function (term, page) {
                    return {
                        q: term, // search term
                        page_limit: 50,
                    };
                },
                results: function (data, page) { // parse the results into the format expected by Select2.
                    // since we are using custom formatting functions we do not need to alter remote JSON data
                    return {
                        results: data.result
                    };
                }
            },
            initSelection: function(element, callback) {
                // the input tag has a value attribute preloaded that points to a preselected repository's id
                // this function resolves that id attribute to an object that select2 can render
                // using its formatResult renderer - that way the repository name is shown preselected
                var id = $(element).val();
                if (id !== "") {
                    $.ajax("http://localhost/git/AgroERP/contract/get_pricepoint_by_id/" + id,
                    {
                        dataType: "json"
                    }).done(function(data) { 
                        callback(data);
                    });
                }
            },
            formatResult: commodityFormatResult, // omitted for brevity, see the source of this page
            formatSelection: commodityFormatSelection, // omitted for brevity, see the source of this page
            dropdownCssClass: "bigdrop", // apply css that makes the dropdown taller
            id: function(item) {
                return item['Id'];
            },
            escapeMarkup: function (m) {
             return m;
            } // we do not want to escape markup since we are displaying html in results
        });
        }
        return {
        //main function to initiate the module
        init: function () {            
            handleSelect2();
            handleSelect2Modal();
        }
    };

}();